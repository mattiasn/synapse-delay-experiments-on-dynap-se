# Synapse-Delay Experiments on DYNAP-SE

A custom cAER module for performing experiments of excitatory-inhibitory (E-I) disynaptic delays and Monte Carlo circuit-choice optimization on the DYNAP-SE mixed-signal neuromorphic processor. Used for the publications "Synaptic Delays for Insect-Inspired Temporal Feature Detection in Dynamic Neuromorphic Processors" (Sandin and Nilsson, 2020) and "Synaptic Integration of Spatiotemporal Features with a Dynamic Neuromorphic Processor" (Nilsson et al., 2020).

This code is quite old and poorly structured, and it uses a discontinued framework. For a better example of working with the DYNAP-SE, see [Nilsson 2020 Spatiotemporal Pattern Recognition](https://gitlab.com/mattiasn/nilsson-2022-spatiotemporal-pattern-recognition).
