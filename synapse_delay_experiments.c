/*
 * A custom module for the software framework cAER
 * for running experiments of Monte Carlo circuit-choice optimization
 * and E-I synapse delays on the DYNAP-SE mixed-signal neuromorphic processor.
 * 
 * 		Author: Mattias Nilsson
 */


// PROGRAM SETUP

// neural architecture
#define CRICKET_NETWORK_1		0
#define CRICKET_NETWORK_2		0
#define PIR_MEASUREMENT			1

// topology initialization
#define RAND_TOPOLOGY				0		// initiate network with randomly generated topology (addresses)
#define FIXED_TOPOLOGY			1		// run network with fixed topology (addresses)

// Monte Carlo optimization
#define MONTE_CARLO					0		// run Monte Carlo optimization algorithm

// spike generation (outdated?)
#define SPIKES_FROM_FILE		0
#define GENERATE_SPIKES			0

// Poisson spike-generation (outdated?)
#define CLONE_POISSON				0
#define POISSON_STEREO 			0 		// use stereo stimuli with Poisson distributed interspike interval
#define FIXED_ISI 					0 		// use stimuli with fixed interspike interval
#define CONST_TRAIN					0

// stimulation paradigm
#define PULSE_STIM					0
#define IND_PULSE_GEN				0 // individual pulse generation for pulse-pairs (?)

#define PIR_PULSE_STIM			1
#define IND_PIR_GEN					1

#define STIM_TEST						0
#define FIRST_SPKTRN_ONLY		1


// OTHER DEFINITIONS

#define SQUARED(x)			(x)*(x)


// INCLUSIONS

#include "base/mainloop.h"
#include "base/module.h"
#include "ext/buffers.h"
#include "ext/portable_time.h"
#include "ext/colorjet/colorjet.h"
#include <time.h>
#include <libcaer/devices/dynapse.h>
#include <libcaer/events/spike.h>
//#include <libcaer/events/frame.h> //display
#include "modules/ini/dynapse_utils.h"

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <limits.h>

struct optimizer_state {
	sshsNode dynapseConfigNode;
	simple2DBufferFloat frequencyMap;
	simple2DBufferLong spikeCountMap;
	//float measureMinTime;
	bool startedMeasure;
	double measureStartedAt;

	caerDeviceHandle eventSourceModuleState;
	sshsNode eventSourceConfigNode;

	// user parameters
	bool changeLN3;
	uint16_t neuronAddr_LN3;
	bool changeLN4;
	uint16_t neuronAddr_LN4;

	bool init;
	bool setbias;
};

typedef struct optimizer_state *optimizerState;

struct caer_dynapse_info dynapseInfo;


// FUNCTION DECLARATIONS

// module functions
static bool caerOptimizerInit(caerModuleData moduleData);
static void caerOptimizerRun(caerModuleData moduleData, caerEventPacketContainer in, caerEventPacketContainer *out);
static void caerOptimizerConfig(caerModuleData moduleData);
static void caerOptimizerExit(caerModuleData moduleData);
static void caerOptimizerReset(caerModuleData moduleData, int16_t resetCallSourceID);

// randomisation functions
double ranf();
void randomise(int seed);

// neural network functions
void randNetworkAddr();
uint16_t randAddrGen(int neuronIndex);
void fixedNetworkAddr();
void setConnectionLists();
void listConnection(int inputNum, int preNeuronIndex, int neuronIndex, int synapseType);
void printNetworkMeanrates(optimizerState state);
void printNetworkSpikeCounts(optimizerState state);
void updateFrequencyMap(optimizerState state);
void updateSpikeCount(optimizerState state);
void writeNetworkConnections(optimizerState state);
uint16_t getGlobalX(uint16_t neuronId);
uint16_t getGlobalY(uint16_t neuronId);
void monitorNeuron(optimizerState state, uint16_t neuronId);

// test functions
void printTestMeanrates(optimizerState state);
void writeTestConnections(optimizerState state);

// PIR measurement functions
void writePIRConnections(optimizerState state);
void changePirCams(optimizerState state);
void writePolyPairConnections(optimizerState state);
void writePolyTripletConnections(optimizerState state);

// biasing functions
void silentBiases(optimizerState state);
void softSpikeGenBiases(optimizerState state);
void lowPowerBiases(optimizerState state);
void workingBiases(optimizerState state);
void customNicolettaBiases(optimizerState state);
void nicolettaBiases(optimizerState state);
void setBiases_LN2(optimizerState state);
void setBiases_LN3(optimizerState state);
void setBiases_LN4(optimizerState state);
void setBiases_LN3_100ms(optimizerState state);
void setBiases_PIR(optimizerState state);
void setBiases_PIR_100ms(optimizerState state);
void setBiases_PIR_100ms_bottom(optimizerState state);
void setBiases_polyPair(optimizerState state);
void setBiases_polyTriplet(optimizerState state);

// stimulation functions
void runStimVariableIsi(optimizerState state, uint32_t stimCountVar);
void runStimFixedIsi(optimizerState state, uint16_t *spikeTrain);
void writeChipSram(optimizerState state);
uint32_t genPoissonSpikeTrain(uint16_t *spikeTrain ,uint16_t inAddr_adj, uint16_t inAddr_opp, uint8_t r_adj, uint8_t r_opp);
uint32_t genPulseSpikeTrain(uint16_t *spikeTrain, uint16_t inputAddr, bool doublePulse);
uint32_t genPoissonPulseSpikeTrain(uint16_t *spikeTrain, uint16_t inputAddr);
uint32_t genConstantSpikeTrain(uint16_t *spikeTrain);
uint32_t genPulsePair(uint16_t *spikeTrain, uint16_t inputAddr, bool doublePulse, float IPI);
uint32_t genSingleSpike(uint16_t *spikeTrain, uint16_t inputAddr);
uint32_t genPolyPairTrain(uint16_t *spikeTrain);
uint32_t genPolyPair(uint16_t *spikeTrain, float polyISI);
uint32_t genPolyTripletTrain(uint16_t *spikeTrain);
uint32_t genPolyTriplet(uint16_t *spikeTrain, float polyISI);
void writeSpikeTrainFixedIsi();
void startStereoStim(optimizerState state, uint16_t *spikeTrain_L, uint16_t *spikeTrain_R);
void startPulseStim(optimizerState state, uint16_t *spikeTrain_1, uint16_t *spikeTrain_2);
void endStereoStim(optimizerState state);
void endPulseStim(optimizerState state);
void printSpikeTrain(uint16_t *spikeTrain, uint32_t stimCount, char *fileName);
uint32_t variableIsiFileToSpikeTrain(uint16_t *spikeTrain, char* fileName);

// Monte Carlo functions
void mcExchangeCam(optimizerState state);
void mcInit(optimizerState state);
void mcEvaluate(optimizerState state);
void mcChangeNeuronAddr(optimizerState state);
void mcResetNeuronAddr(optimizerState state);
void mcObjectiveFunction();

static struct caer_module_functions caerOptimizerFunctions =
{
		.moduleInit = &caerOptimizerInit,
		.moduleRun = &caerOptimizerRun,
		.moduleConfig = &caerOptimizerConfig,
		.moduleExit = &caerOptimizerExit,
		.moduleReset = &caerOptimizerReset
};

static const struct caer_event_stream_in moduleInputs[] =
{
		{ .type = SPIKE_EVENT, .number = 1, .readOnly = true }
};

static const struct caer_module_info moduleInfo =
{
		.version = 1,
		.name = "Optimizer",
		.description = "Network optimizer",
		.type = CAER_MODULE_OUTPUT,
		.memSize = sizeof(struct optimizer_state),
		.functions = &caerOptimizerFunctions,
		.inputStreams = moduleInputs,
		.inputStreamsSize = CAER_EVENT_STREAM_IN_SIZE(moduleInputs),
		.outputStreams = NULL,
		.outputStreamsSize = 0
};

caerModuleInfo caerModuleGetInfo(void)
{
	return (&moduleInfo);
}

// declare file pointers
FILE *spikeCountFile;
FILE *networkAddrFile;
FILE *fixedAddrFile;

// random number generator parameters
int arr[60];
int next;
int nextp;

#if CRICKET_NETWORK_1
#define numInput 		2
#define numMid 			2
#define numMotor 		2
#define numNeur 		6
#define numSyn 			64
#endif // CRICKET_NETWORK_1

#if CRICKET_NETWORK_2 || PIR_MEASUREMENT

#define numInput 		1
#define numMid 			2
#define numMotor 		1
#define numNeur 		4
#define numSyn 			64

#endif // CRICKET_NETWORK_1

#define coreId_LN2		2
#define coreId_LN3		1
#define coreId_LN4		3

uint16_t neuronAddr[numNeur]; // list of network neuron addresses
uint16_t inputAddrList[numNeur][numSyn]; // list of lists of input neuron addresses
uint16_t lastNeuronAddr; // store replaced neuron address for Monte Carlo
uint8_t synapseTypeList[numNeur][numSyn]; // 0 = S_INH, 1 = F_INH, 2 = S_EXC, 3 = F_EXC, 4 = no connection

uint8_t camId_PIR_sExc = 0;
uint8_t camId_PIR_fInh = 1;
int camEpoch = 1;
int camIteration = 0;

uint16_t selectedNeurons_LN3[] = {10, 24, 25, 27, 46, 48, 123, 138, 190, 224};
int monitoringIndex_LN3 = 0;

// network neuron indices

#if CRICKET_NETWORK_1

int inLF_L = 0;
int inLF_R = 1;

int ON1_L = 2;
int ON1_R = 3;

int AN1_L = 4;
int AN1_R = 5;

const char *neuronNames[] = {"inLF_L", "inLF_R", "ON1_L", "ON1_R", "AN1_L", "AN1_R"};

#endif // CRICKET_NETWORK_1

#if CRICKET_NETWORK_2 || PIR_MEASUREMENT

int AN1 = 0;	// ascending neuron
int LN2 = 1;	// inhibitory neuron
int LN3 = 2;	// coincidence neuron (with "integrated" delay neuron)
int LN4 = 3;	// feature detector

const char *neuronNames[] = {"AN1", "LN2", "LN3", "LN4"};

#endif // CRICKET_NETWORK_2


// MONTE CARLO PARAMETERS

uint32_t sweepInd = 1; // Monte Carlo sweep index
uint16_t mcPos = numInput; // Monte Carlo sweep neuron position - start after input neurons
bool mcTest = false;
bool mcStop = false;
uint16_t inputAddr1;
uint16_t inputAddr2;
uint8_t synType1; // synapse type (for exchange)
uint8_t synType2;
uint8_t camId1; // synapse CAM-ID (for exchange)
uint8_t camId2;
bool changeKept;


// MEAN-RATE PARAMETERS

float measureMinTime = 1;

// left motor
float meanRate_L; // meanrate
float lastMeanRate_L; // old meanrate
float rateDiff_L;
float lastRateDiff_L;
bool decrLeft; // decrease of meanrate

//right motor
float meanRate_R; // meanrate
float lastMeanRate_R; // old meanrate
float rateDiff_R;
float lastRateDiff_R;
bool decrRight; // decrease of meanrate

//float minMeasTime = 0.5; // min. measurement time [s]
uint32_t measurementCounter = 0; // count number of performed meanrate measurements


// SPIKE GENERATOR PARAMETERS

//#define stimTime			1

#define dt					1e-6 // time step [s]
// #define possonStimTime 			1
//#define mcInitTime			1
#define firingRateLF_high	110 //110 // instantenous firing rate [spikes/s] - low frequency
#define firingRateLF_low	100 //100 // instantenous firing rate [spikes/s] - low frequency
#define spikeTrainLength	10000 // spike train (buffer) length - not the same as stimCount
//uint32_t spikeTrainLength = round(1.5 * stimTime * (firingRateLF_high+firingRateLF_low));
//int timeStepNum = stimTime/dt; // number of time steps
uint16_t clockFreq = 90; // main logic system clock [MHz]

#if POISSON_STEREO
#define stimTime			1 // possonStimTime
#endif // POISSON_STEREO

//#define pulseStimTime		500e-3
#define pulseStartTime		10e-3
#define pulseDuration 		20e-3	// [s]
#define pulseInterval		20e-3	// [s]
#define perturbRate			0		// 0.1 --> +-10 %

#if PULSE_STIM
	#if IND_PULSE_GEN
		#define stimTime			1 //0.300
	#else
		#define stimTime			2.5 // 500e-3 // 1 // pulseStimTime
	#endif // IND_PULSE_GEN

	#define pulseFiringRate 	500 // 150 // 500		// [Hz]

bool monitoring = true; // no PIR-tuning monitoring
#endif // PULSE_STIM

#if PIR_PULSE_STIM
	#if IND_PIR_GEN
		#define stimTime			0.1
	#else
		#define stimTime			1.5 // 5 	// [s]
	#endif

	#define pulseFiringRate 	150 	// [Hz]

bool monitoring = false; // activate monitoring for PIR-tuning
#endif // PIR_PULSE_STIM

// variable ISI stimuli
uint32_t isiFactorVar; // inter-spike interval factor - in terms of base multiplier
uint32_t isiBaseVar = 900; // ISI base multiplier | one isiBase = 1/(90 Mhz) = 11.11 ns | 90/90 MHz = 1 us
bool varModeVar = true; // use variable inter-spike intervals
uint32_t baseAddrVar = 0; // FPGA SRAM base address for programming spike train [0,1024]
//uint32_t stimCountVar; // number of lines (addresses) for spike generator

bool repeatVar = false; // repeat stimulation once finished
bool runVar = true; // start/stop stimulation - will finish a complete stimulation before ending

// fixed ISI stimuli
uint32_t isiFactorFixed = 200e3; //[us] | 2e5 us = 2e5 * 1e-6 s = 0.200 s = 200 ms
uint32_t isiBaseFixed = 90;
bool varModeFixed = false;
uint32_t baseAddrFixed = 0;
uint32_t stimCountFixed = 2;
bool repeatFixed = false;
bool runFixed = false;

bool stim_L = false;
bool stim_R = false;
bool genTrain_L = false;
bool genTrain_R = false;
bool readTrain_L = false;
bool readTrain_R = false;
uint32_t stimCount_L;
uint32_t stimCount_R;
//uint16_t spikeTrain;
uint16_t spikeTrainFixed[2];
uint16_t spikeTrainLF_L[spikeTrainLength]; // low frequency spike train incident from the left
uint16_t spikeTrainLF_R[spikeTrainLength]; // low frequency spike train incident from the right
uint16_t spikeTrain_singlePulse[2*1024];
uint16_t spikeTrain_doublePulse[2*1024];
//uint32_t *isiTrain_LF; // interspike intervals for the spike train
uint16_t stimCycles = 0; // stimulus measurement cycles counter
uint32_t stimRuns = 0; // count total number of stimulus runs

bool stim_singlePulse = false;
bool stim_doublePulse = false;
bool genTrain_1 = false;
bool genTrain_2 = false;
uint32_t stimCount_1;
uint32_t stimCount_2;

float pulseIntervals[] = {1/(float)pulseFiringRate, 10e-3, 20e-3, 30e-3, 40e-3, 50e-3}; // [s]
float pairISIs[] = {0, 1e-3, 2e-3, 3e-3, 4e-3, 5e-3, 6e-3, 7e-3, 8e-3, 9e-3, 10e-3}; // [s]
//float tripletISIs[] = {0, 0.5e-3, 1e-3, 1.5e-3, 2e-3, 2.5e-3, 3e-3, 3.5e-3, 4e-3, 4.5e-3, 5e-3}; // [s]
float tripletISIs[] = {0, 1e-3, 2e-3, 3e-3, 4e-3, 5e-3, 6e-3, 7e-3, 8e-3, 9e-3, 10e-3}; // [s]
int ipiNum = 6;
int ipiIndex = 0;
int pulsePairCounter = 0;

int wInh[] = {145, 150, 155};
int wExc[] = {210, 140, 70};
int wIndex = 0;
int numWeights = 3;

bool startedStim = false;
double stimStartedAt;

double biasesSetAt;

bool startedFixedStim = false;

bool pirHold = true;

// FPGA SRAM PARAMETERS | low freq. stimulus  -	LOOP HERE?

uint8_t fpgaVirtualSourceChip = 0; // [0,3]
uint8_t fpgaDestinationCore = 15; // [0, 15] | destination chip is always U0
uint16_t fpgaNeuronAddrPre;
uint16_t fpgaNeuronAddrPreDelay;

#define targetRateAdj		100	// target frequency of motor neuron adjacent to direction of incidence of stimulus
#define targetRateOpp		50	// target frequency of motor neuron opposite to direction of incidence of stimulus
#define diffSumThreshold	1


// SPIKE-COUNT PARAMETERS

float spikeCount_L;
float spikeCount_R;
float countDiff_L; // to be maximized
float countDiff_R;
float countDiffSum;
float lastCountDiffSum;
float targetDiff_L; // to be minimized
float targetDiff_R;
float targetDiffSum;
float lastTargetDiffSum;
bool incrTargetDiffSum;
bool decrCountDiffSum;
float targetCountAdj = targetRateAdj * stimTime;
float targetCountOpp = targetRateOpp * stimTime;

float spikeCount_LN2;
float spikeCount_LN3;
float spikeCount_LN4;
float targetDiff_singlePulse;
float targetDiff_doublePulse;

float targetCount_LN2_singlePulse = 4; // single pulse stimulus
float targetCount_LN2_doublePulse = 8; // double pulse stimulus

float targetCount_LN3_singlePulse = 2; // single pulse stimulus
float targetCount_LN3_doublePulse = 6; // double pulse stimulus

float targetCount_LN4_singlePulse = 0; // single pulse stimulus
float targetCount_LN4_doublePulse = 1; // double pulse stimulus

uint16_t pirOutputId = 0;


// MODULE FUNCTIONS

static bool caerOptimizerInit(caerModuleData moduleData) {

	optimizerState state = moduleData->moduleState;

	// Wait for input to be ready. All inputs, once they are up and running, will
	// have a valid sourceInfo node to query, especially if dealing with data.
	int16_t *inputs = caerMainloopGetModuleInputIDs(moduleData->moduleID, NULL);
	if (inputs == NULL) {
		return (false);
	}

	int16_t sourceID = inputs[0];
	free(inputs);

	//void sshsNodeCreateFloat(sshsNode node, const char *key, float defaultValue, float minValue, float maxValue, int flags, const char *description) CAER_SYMBOL_EXPORT;

	// create user parameters
	//sshsNodeCreateFloat(moduleData->moduleNode, "measureMinTime", 1, 0.001f, 300, SSHS_FLAGS_NORMAL, "Measure time before updating the mean (in seconds).");
	sshsNodeCreateBool(moduleData->moduleNode, "ChangeLN3", false, SSHS_FLAGS_NORMAL, "Change the neuron address for LN3.");
	sshsNodeCreateInt(moduleData->moduleNode, "neuronAddress_LN3", 1, 1, 255, SSHS_FLAGS_NORMAL, "Neuron address used for LN3");
	sshsNodeCreateBool(moduleData->moduleNode, "ChangeLN4", false, SSHS_FLAGS_NORMAL, "Change the neuron address for LN4.");
	sshsNodeCreateInt(moduleData->moduleNode, "neuronAddress_LN4", 1, 1, 255, SSHS_FLAGS_NORMAL, "Neuron address used for LN4");

	// update node state
	state->changeLN3 = sshsNodeGetBool(moduleData->moduleNode, "ChangeLN3");
	state->neuronAddr_LN3 = (uint16_t)sshsNodeGetInt(moduleData->moduleNode, "neuronAddress_LN3");
	state->changeLN4 = sshsNodeGetBool(moduleData->moduleNode, "ChangeLN4");
	state->neuronAddr_LN4 = (uint16_t)sshsNodeGetInt(moduleData->moduleNode, "neuronAddress_LN4");

	sshsNode sourceInfoSource = caerMainloopGetSourceInfo(sourceID);
	if (sourceInfoSource == NULL) {
		return (false);
	}

	int16_t sizeX = sshsNodeGetShort(sourceInfoSource, "dataSizeX");
	int16_t sizeY = sshsNodeGetShort(sourceInfoSource, "dataSizeY");

	state->frequencyMap = simple2DBufferInitFloat((size_t) sizeX, (size_t) sizeY);
	if (state->frequencyMap == NULL) {
		return (false);
	}

	state->spikeCountMap = simple2DBufferInitLong((size_t) sizeX, (size_t) sizeY);
	if (state->spikeCountMap == NULL) {
		return (false);
	}

	sshsNode sourceInfoNode = sshsGetRelativeNode(moduleData->moduleNode, "sourceInfo/");
	sshsNodeCreateShort(sourceInfoNode, "dataSizeX", sizeX, 1, 1024, SSHS_FLAGS_READ_ONLY | SSHS_FLAGS_NO_EXPORT,
			"Output data width.");
	sshsNodeCreateShort(sourceInfoNode, "dataSizeY", sizeY, 1, 1024, SSHS_FLAGS_READ_ONLY | SSHS_FLAGS_NO_EXPORT,
			"Output data height.");

	caerOptimizerConfig(moduleData);

	state->dynapseConfigNode = caerMainloopGetSourceNode(sourceID);

	// internals
	state->init = false;
	state->setbias = false;

	// open files
	spikeCountFile = fopen("spikeCount.txt", "w");
	networkAddrFile = fopen("networkAddresses.txt", "w");
	fixedAddrFile = fopen("fixedAddresses.txt", "r");

	// Add config listeners last, to avoid having them dangling if Init doesn't succeed.
	sshsNodeAddAttributeListener(moduleData->moduleNode, moduleData, &caerModuleConfigDefaultListener);

	// Nothing that can fail here.
	return (true);
}

static void caerOptimizerRun(caerModuleData moduleData, caerEventPacketContainer in, caerEventPacketContainer *out) {
	caerSpikeEventPacketConst spike = (caerSpikeEventPacketConst) caerEventPacketContainerFindEventPacketByTypeConst(in,
			SPIKE_EVENT);

	// Only process packets with content.
	if (spike == NULL) {
		//printf("No events.\n");
		return;
	}

	optimizerState state = moduleData->moduleState;

	// now we can do crazy processing etc..
	// first find out which one is the module producing the spikes. and get usb handle
	// --- start  usb handle / from spike event source id
	int sourceID = caerEventPacketHeaderGetEventSource(&spike->packetHeader);
	state->eventSourceModuleState = caerMainloopGetSourceState(U16T(sourceID));
	state->eventSourceConfigNode = caerMainloopGetSourceNode(U16T(sourceID));
	if (state->eventSourceModuleState == NULL || state->eventSourceConfigNode == NULL) {
		return;
	}

	//initialization
	if (state->init == false) {

		caerLog(CAER_LOG_NOTICE, __func__, "Initialization of custom network");

		// get dynapse device information
		dynapseInfo = caerDynapseInfoGet(state->eventSourceModuleState);

		int seed = time(NULL); // get seed from system clock
		randomise(seed);

		/*// perform full initialization of the device and its chips - to work with it reliably
		bool defaultConf = caerDeviceSendDefaultConfig(state->eventSourceModuleState);
		if (defaultConf){
			printf("Full initialization of device and chips performed.\n");
		}
		else {
			printf("Full initialization of device and chips failed.\n");
		}*/

		// load silent biases while configuring, to speed up configuration
		silentBiases(state);
		caerLog(CAER_LOG_NOTICE, __func__, "Silent biases loaded to speed up configuration");

#if RAND_TOPOLOGY
		// generate neuron addresses on the board (randomly)
		randNetworkAddr();
		caerLog(CAER_LOG_NOTICE, __func__, "Random neural network addresses generated.");
#elif FIXED_TOPOLOGY
		// assign fixed neuron addresses
		fixedNetworkAddr();
		caerLog(CAER_LOG_NOTICE, __func__, "Fixed neural network addresses read.");
#endif // RAND_TOPOLOGY

#if PIR_MEASUREMENT
		neuronAddr[AN1] = 1;
#endif // PIR_MEASUREMENT

		// setup lists of network connections
		setConnectionLists();
		caerLog(CAER_LOG_NOTICE, __func__, "Lists of neural connections set.");

		// Select chip to operate on
		caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_CHIP, DYNAPSE_CONFIG_CHIP_ID, DYNAPSE_CONFIG_DYNAPSE_U0);

		// Clear all cam for that particular chip
		//bool dynapseConfigSet(caerDeviceHandle cdh, int8_t modAddr, uint8_t paramAddr, uint32_t param) | modAddr = DYNAPSE_CONFIG_CLEAR_CAM
		caerLog(CAER_LOG_NOTICE, __func__, "Started clearing CAM ");
		//caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_CLEAR_CAM, DYNAPSE_CONFIG_DYNAPSE_U0, 0);
		caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_CLEAR_CAM, 0, 0);
		caerLog(CAER_LOG_NOTICE, __func__, "CAM cleared");

		// program initial connections
#if PIR_MEASUREMENT
		writePIRConnections(state);
		//writePolyPairConnections(state);
		//writePolyTripletConnections(state);
#else
		writeNetworkConnections(state);
#endif // PIR_MEASUREMENT
		//writeTestConnections(state);

		// set working biases
		//softSpikeGenBiases(state);
		//lowPowerBiases(state);
		//workingBiases(state);
		//customNicolettaBiases(state);
		//nicolettaBiases(state);

#if CRICKET_NETWORK_1
		workingBiases(state);
		caerLog(CAER_LOG_NOTICE, __func__, "Biases set for cricket network 1.");
#endif // CRICKET_NETWORK_1

#if CRICKET_NETWORK_2
		monitorNeuron(state, neuronAddr[LN2]);
		monitorNeuron(state, neuronAddr[LN3]);
		monitorNeuron(state, neuronAddr[LN4]);

		setBiases_LN2(state);
		setBiases_LN3(state);
		setBiases_LN4(state);
		caerLog(CAER_LOG_NOTICE, __func__, "Biases set for cricket network 2.");
#endif // CRICKET_NETWORK_2

#if PIR_MEASUREMENT
		lowPowerBiases(state);
		//setBiases_PIR_100ms_bottom(state);
		//setBiases_LN3_100ms(state);
		setBiases_polyPair(state);
		//setBiases_polyTriplet(state);

		caerLog(CAER_LOG_NOTICE, __func__, "Biases set for PIR measurement.");

		// monitor first neuron
		//monitorNeuron(state, coreId_LN3*256 + pirOutputId);
		//pirOutputId++;

		//monitorNeuron(state, coreId_LN3*256 + 22);
#endif

	struct timespec biasTime;
	portable_clock_gettime_monotonic(&biasTime);
	biasesSetAt = (double) biasTime.tv_sec + 1.0e-9 * (double) biasTime.tv_nsec;

		// program on-chip SRAM
#if PIR_MEASUREMENT

#else
		writeChipSram(state);
		caerLog(CAER_LOG_NOTICE, __func__, "On-chip SRAM programmed.");
#endif // PIR_MEASUREMENT

#if CONST_TRAIN
		stimCount_L = genConstantSpikeTrain(spikeTrainLF_L); // left-incident signal
		//printSpikeTrain(spikeTrain_L, stimCount_L, "poissonTrain_L.txt");
		size_t numWords_L = 2 * stimCount_L;
		caerDynapseWriteSramWords(state->eventSourceModuleState, (uint16_t *)spikeTrainLF_L, baseAddrVar, numWords_L); // write FPGA SRAM
		//caerLog(CAER_LOG_NOTICE, __func__, "FPGA SRAM programmed for left-incident stimulus.");
		repeatVar = true;
		runStimVariableIsi(state, stimCount_L); // run left-incident stimulus
#endif // CONST_TRAIN

		caerLog(CAER_LOG_NOTICE, __func__, "init completed");
		state->init = true;
	}
	//sweepInd = 1; // first sweep initiated

	// Iterate over events and update count
	CAER_SPIKE_CONST_ITERATOR_VALID_START(spike)
	uint16_t x = caerDynapseSpikeEventGetX(caerSpikeIteratorElement);
	uint16_t y = caerDynapseSpikeEventGetY(caerSpikeIteratorElement);
	state->spikeCountMap->buffer2d[x][y] += 1;
	CAER_SPIKE_ITERATOR_VALID_END

	//printf("mcTest = %d, mcStop = %d, stimCycles = %d, stimRuns = %d.\n", mcTest, mcStop, stimCycles, stimRuns);
#if MONTE_CARLO
	if (!mcTest && (stimCycles == 0) && (4 <= stimRuns) && !mcStop) { // if system not in state of mcTest
		mcInit(state); // initiate Monte Carlo routine
	}
#endif // MONTE_CARLO

#if POISSON_STEREO
	if (!startedStim) {
		startStereoStim(state, spikeTrainLF_L, spikeTrainLF_R);
	}
#endif // POISSON_STEREO

#if FIXED_ISI
	if (!startedFixedStim){
		writeSpikeTrainFixedIsi(); // write spike train array
		runStimFixedIsi(state, spikeTrainFixed);// run spike generation from FPGA
		startedFixedStim = true;
		caerLog(CAER_LOG_NOTICE, __func__, "Started fixed ISI stimulus.");
	}
#endif // FIXED_ISI

#if PULSE_STIM
	if (!startedStim && !pirHold){
		startPulseStim(state, spikeTrain_singlePulse, spikeTrain_doublePulse);
	}
#endif // PULSE_STIM

#if PIR_PULSE_STIM
	if (!startedStim && !pirHold)
	{
		startPulseStim(state, spikeTrain_singlePulse, spikeTrain_doublePulse);
	}
#endif // PIR_PULSE_STIM

	/*// start measurement
	if (!state->startedMeasure) {
		//caerLog(CAER_LOG_NOTICE, __func__, "Starting measurement.");
		struct timespec tStart;
		portable_clock_gettime_monotonic(&tStart);
		state->measureStartedAt = (double) tStart.tv_sec + 1.0e-9 * (double) tStart.tv_nsec;
		state->startedMeasure = true;

		//printf("Started measuring.\n");
		// measurement initiated
	}*/

	// get current time
	struct timespec tEnd;
	portable_clock_gettime_monotonic(&tEnd);
	double now = (double) tEnd.tv_sec + 1.0e-9 * (double) tEnd.tv_nsec;

	//printf("now = %lf, measureStartedAt = %lf, measureMinTime = %f\n", now, state->measureStartedAt, state->measureMinTime);
	//printf("measurementTime = %f\n", (now - state->measureStartedAt));

	// end measurement
	//if ((now - state->measureStartedAt) >= (double) state->measureMinTime)

#if POISSON_STEREO || PULSE_STIM || PIR_PULSE_STIM

	// start monitoring
	//if (!monitoring && startedStim && (now - stimStartedAt) >= (double) stimTime * 0.5)
	if (!monitoring && startedStim && (now - stimStartedAt) >= (double) stimTime * 0.9)
	{
		monitorNeuron(state, coreId_LN3*256 + pirOutputId);

		pirOutputId++;
		//pirOutputId += 26;

		if (pirOutputId >= 256)
			pirOutputId = 0;

		monitoring = true;
	}

	float stimulationInterval = 250e-3; // [s]

	int pulseCountIndex = 0;
	float pulseCountTime = 20e-3 + stimulationInterval/2;

	// update spike-count (per pulse pair)
	if (startedStim && (now - stimStartedAt) >= pulseCountTime && pulseCountIndex <= 5)
	{
		// get spike-count for current pulse/-pair
		//updateSpikeCount(state);
		//printNetworkSpikeCounts(state);

		//pulseCountIndex++;
		//pulseCountTime += 20e-3 + pulseIntervals[pulseCountIndex] + 20e-3 + stimulationInterval/2;
	}

	// end stimulus
	if (startedStim && (now - stimStartedAt) >= (double) stimTime) {
		//caerLog(CAER_LOG_NOTICE, __func__, "Ending measurement.");

		//printf("stimRuns = %d.\n", stimRuns);
		//printNetworkSpikeCounts(state);
		updateSpikeCount(state);
#if POISSON_STEREO
		endStereoStim(state);
#elif PULSE_STIM || PIR_PULSE_STIM
		endPulseStim(state);

		//// clear CAMs of previous LN3 circuit
		//caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN3], 0, 0, 0);
		//caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN3], 0, 1, 0);
		//caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN3], 0, 2, 0);

		//// change neuron-address of LN3 and monitor the new one
		//neuronAddr[LN3] = coreId_LN3 * 256 + selectedNeurons_LN3[monitoringIndex_LN3];
		//monitorNeuron(state, neuronAddr[LN3]);
		//setConnectionLists();
		//writeNetworkConnections(state);
		//monitoringIndex_LN3++;

		//if (monitoringIndex_LN3 >= 10)
			//monitoringIndex_LN3 = 0;
#endif // POISSON_STEREO/PULSE_STIM

#if PIR_PULSE_STIM
		//changePirCams(state);
		monitoring = false;
#endif // PIR_PULSE_STIM

		stimCycles++;
		//printf("stimCycles = %d.\n", stimCycles);

		if (2 <= stimCycles){
			mcObjectiveFunction();

#if MONTE_CARLO
			if (mcTest && !mcStop){ // if system in state of mcTest - evaluate
				mcEvaluate(state);
			}
			if (targetDiffSum <= diffSumThreshold){
				//mcStop = true;
			}
#endif // MONTE_CARLO

			// write network addresses to new line in file
			for (int i=0; i<numNeur; i++){
				fprintf(networkAddrFile, "%d\t", neuronAddr[i]);
			}
			fprintf(networkAddrFile, "%d\n", changeKept);
			fflush(networkAddrFile);

			//fprintf(spikeCountFile, "%d", changeKept);
			//fflush(spikeCountFile);

			stimCycles = 0;
		}
		fprintf(spikeCountFile, "\n");
		fflush(spikeCountFile);
	} // stimulus ended

	if ((now - biasesSetAt) >= (double) 10)
		pirHold = false;

#endif // POISSON_STEREO || PULSE_STIM || PIR_PULSE_STIM
}

static void caerOptimizerConfig(caerModuleData moduleData) {

	caerModuleConfigUpdateReset(moduleData);

	optimizerState state = moduleData->moduleState;

	// update parameters from user input
	//state->measureMinTime = sshsNodeGetFloat(moduleData->moduleNode, "measureMinTime");

	bool newLN3 = sshsNodeGetBool(moduleData->moduleNode, "ChangeLN3");
	bool newLN4 = sshsNodeGetBool(moduleData->moduleNode, "ChangeLN4");

	if (newLN3 && !state->changeLN3)
	{
		state->changeLN3 = true;

		state->neuronAddr_LN3 = (uint16_t)sshsNodeGetInt(moduleData->moduleNode, "neuronAddress_LN3");

		// clear CAMs of previous LN3 circuit
		caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN3], 0, 0, 0);
		caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN3], 0, 1, 0);
		caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN3], 0, 2, 0);

		// change neuron-address of LN3 and monitor the new one
		neuronAddr[LN3] = coreId_LN3 * 256 + state->neuronAddr_LN3;
		monitorNeuron(state, neuronAddr[LN3]);
		setConnectionLists();
		writeNetworkConnections(state);
	}
	else if(!newLN3 && state->changeLN3)
	{
		state->changeLN3 = false;
	}

	if (newLN4 && !state->changeLN4)
	{
		state->changeLN4 = true;

		state->neuronAddr_LN4 = (uint16_t)sshsNodeGetInt(moduleData->moduleNode, "neuronAddress_LN4");

		// clear CAMs of previous LN3 circuit
		caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN4], 0, 0, 0);
		caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[LN4], 0, 1, 0);

		// change neuron-address of LN3 and monitor the new one
		neuronAddr[LN4] = coreId_LN4 * 256 + state->neuronAddr_LN4;
		monitorNeuron(state, neuronAddr[LN4]);
		setConnectionLists();
		writeNetworkConnections(state);
	}
	else if(!newLN4 && state->changeLN4)
	{
		state->changeLN4 = false;
	}
}

static void caerOptimizerExit(caerModuleData moduleData) {

	optimizerState state = moduleData->moduleState;

	// close open files
	fclose(spikeCountFile);
	fclose(networkAddrFile);
	fclose(fixedAddrFile);

	// Remove listener, which can reference invalid memory in userData.
	sshsNodeRemoveAttributeListener(moduleData->moduleNode, moduleData, &caerModuleConfigDefaultListener);

	sshsNode sourceInfoNode = sshsGetRelativeNode(moduleData->moduleNode, "sourceInfo/");
	sshsNodeClearSubTree(sourceInfoNode, true);

	// Ensure maps are freed.
	simple2DBufferFreeFloat(state->frequencyMap);
	simple2DBufferFreeLong(state->spikeCountMap);
}

static void caerOptimizerReset(caerModuleData moduleData, int16_t resetCallSourceID) {
	UNUSED_ARGUMENT(resetCallSourceID);

	optimizerState state = moduleData->moduleState;

	// Reset maps to all zeros (startup state).
	simple2DBufferResetFloat(state->frequencyMap);
	simple2DBufferResetLong(state->spikeCountMap);
}


// RANDOMIZATION FUNCTIONS

double ranf(){
	int i1,ib,ibig;
	double fak;

	/*   fak = 2.0**(-31); */
	fak = 1.0 / 2147483648.0 ;
	ib = 1073741824;
	ibig = 2*ib;
	next=next-1;
	if (next == 0 ) next = 55;
	nextp=nextp-1;
	if (nextp == 0 ) nextp = 55;
	arr[next] = arr[next] - arr[nextp];
	if ( arr[next] < 0 ) arr[next] = arr[next]+ibig;
	return(fak*arr[next]);
}

void randomise(int seed){ // Function: Initialize random generator called by vranf(), uses variable 'seed' as starter.
	int i1,ii,ib,ibig,j,k;
	//int seed;
	double fak,ranf(),x1;

	/*   fak = 2.0**(-31); */
	fak = 1.0 / 2147483648.0 ;
	ib = 1073741824;
	ibig = 2*ib;
	//seed = 233993;
	j = 43587627 + seed;
	if (j < 0) j=j+ibig;
	arr[1]=j;
	k=1;
	for (i1=1;i1<=54;i1++) {
		ii=56-((21*i1)%55);
		arr[ii]=k;
		k=j-k;
		if ( k < 0 ) k = k + ibig;
		j = arr[ii];
	}
	next=56;
	nextp=25;
	for (i1=1;i1<=220;i1++) {
		x1=ranf();
	}
}

void randNetworkAddr(){ // set up neural network with random addresses on the board

	// fixed virtual input addresses
	for (int i=0; i<numInput; i++){
		neuronAddr[i] = i+1; // don't use address 0 - not a valid source address}
	}

	// initialize random number generator
	//int seed = time(NULL); // get seed from system clock
	//randomise(seed);

	bool duplicate; // duplicate detection indicator
	int randStartInd = numInput; // don't generate random addresses for (virtual) input neurons

	// loop over network neurons
	for (int i=randStartInd; i<numNeur; i++){

		// middle layer and motor neurons | C1-C3
		neuronAddr[i] = randAddrGen(i);

		// avoid creating duplicates
		duplicate = true;
		// check for duplicates and generate new address until neuron address i is unique - not for i=0
		while (i>randStartInd && duplicate){
			// loop over previously generated addresses
			for (int j=randStartInd; j<i; j++){
				// check for duplicate
				if (neuronAddr[i] == neuronAddr[j]){
					// generate new address i - middle/motor neurons
					neuronAddr[i] = randAddrGen(i);
					break; // terminate loop over assigned addresses - while-loop continues
				}
			}
			duplicate = false; // no duplicate was found for address i
		}
	}
	// print generated random neuron addresses
	for (int i=0; i<numNeur; i++){
		fprintf(networkAddrFile, "%d\t", neuronAddr[i]);
		printf("neuronAddr %d = %d.\n", i, neuronAddr[i]);
	}
	fprintf(networkAddrFile, "\n");
	fflush(networkAddrFile);
}

uint16_t randAddrGen(int neuronIndex){ // generate random address on the board for one neuron

	uint16_t randAddr;

#if CRICKET_NETWORK_1
	randAddr = floor(768*ranf()) + 256; // floor([0,1023)) + 1 --> [1,1023]
#endif // CRICKET_NETWORK_1

#if CRICKET_NETWORK_2
	// network neurons are confined to different cores
	if (neuronIndex == LN2)
		randAddr = floor(256 * ranf()) + coreId_LN2 * 256;
	else if (neuronIndex == LN3)
		randAddr = floor(256 * ranf()) + coreId_LN3 * 256;
	else if (neuronIndex == LN4)
		randAddr = floor(256 * ranf()) + coreId_LN4 * 256;
	else
		caerLog(CAER_LOG_NOTICE, __func__, "Invalid neuron index!");
#endif // CRICKET_NETWORK_2

	return(randAddr);
}


// DYNAP-SE NEURAL NETWORK FUNCTIONS

void fixedNetworkAddr()
{
	caerLog(CAER_LOG_NOTICE, __func__, "Initiating fixedNetworkAddr.");

	/*uint16_t fixedAddr;
	int i = 0;
	while (fscanf(fixedAddrFile, "%d", fixedAddr) != EOF)
	{
		neuronAddr[i] = fixedAddr;
		i++;
		printf("scanCount = %d", i);
		if (i > numNeur)
			caerLog(CAER_LOG_NOTICE, __func__, "Scanning too many entries.");
	}*/
#if CRICKET_NETWORK_1
	neuronAddr[0] = 1;
	neuronAddr[1] = 2;

	neuronAddr[2] = 754;
	neuronAddr[3] = 864;

	neuronAddr[4] = 826;
	neuronAddr[5] = 871;

#elif CRICKET_NETWORK_2
	neuronAddr[AN1] = 1;
	neuronAddr[LN2] = coreId_LN2 * 256 + 1;
	neuronAddr[LN3] = coreId_LN3 * 256 + 117;
	neuronAddr[LN4] = coreId_LN4 * 256 + 1;
#endif
	caerLog(CAER_LOG_NOTICE, __func__, "fixedNetworkAddr executed.");
}

void setConnectionLists(){ // setup lists for neural connections

	// initiate all neurons with no input connections
	for (int i=0; i<numNeur; i++){
		for(int j=0; j<numSyn; j++){
			inputAddrList[i][j] = 0;
			synapseTypeList[i][j] = 4;
		}
	}
#if CRICKET_NETWORK_1

	// ON1 left
	listConnection(0, inLF_L, ON1_L, DYNAPSE_CONFIG_CAMTYPE_F_EXC);
	//listConnection(1, inHF_L, ON1_L, DYNAPSE_CONFIG_CAMTYPE_F_EXC);
	listConnection(1, ON1_R, ON1_L, DYNAPSE_CONFIG_CAMTYPE_F_INH);

	// ON1 right
	listConnection(0, inLF_R, ON1_R, DYNAPSE_CONFIG_CAMTYPE_F_EXC);
	//listConnection(1, inHF_R, ON1_R, DYNAPSE_CONFIG_CAMTYPE_F_EXC);
	listConnection(1, ON1_L, ON1_R, DYNAPSE_CONFIG_CAMTYPE_F_INH);

	// AN1 left
	listConnection(0, inLF_L, AN1_L, DYNAPSE_CONFIG_CAMTYPE_F_EXC);
	listConnection(1, ON1_R, AN1_L, DYNAPSE_CONFIG_CAMTYPE_F_INH);

	// AN1 right
	listConnection(0, inLF_R, AN1_R, DYNAPSE_CONFIG_CAMTYPE_F_EXC);
	listConnection(1, ON1_L, AN1_R, DYNAPSE_CONFIG_CAMTYPE_F_INH);

#endif //CRICKET_NETWORK_1

#if CRICKET_NETWORK_2

	// LN2
	listConnection(0, AN1, LN2, DYNAPSE_CONFIG_CAMTYPE_F_EXC);

	// LN3
	listConnection(0, AN1, LN3, DYNAPSE_CONFIG_CAMTYPE_F_EXC);
	listConnection(1, LN2, LN3, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
	listConnection(2, LN2, LN3, DYNAPSE_CONFIG_CAMTYPE_F_INH);

	// LN4
	listConnection(0, LN2, LN4, DYNAPSE_CONFIG_CAMTYPE_F_INH);
	listConnection(1, LN3, LN4, DYNAPSE_CONFIG_CAMTYPE_F_EXC);

#endif //CRICKET_NETWORK_1
}

void listConnection(int inputNum, int preNeuronIndex, int neuronIndex, int synapseType){
	inputAddrList[neuronIndex][inputNum] = neuronAddr[preNeuronIndex];
	synapseTypeList[neuronIndex][inputNum] = synapseType;
	//printf("inputNum = %d, preNeuronIndex = %d, neuronIndex = %d, synapseType = %d.\n", inputNum, preNeuronIndex, neuronIndex, synapseType);
}

void writeNetworkConnections(optimizerState state){
	//caerLog(CAER_LOG_NOTICE, __func__, "Started programming CAM for input stimulus");

	for (uint16_t i=numInput; i<numNeur; i++){ // loop over network neurons
		for (uint8_t j=0; j<numSyn; j++){ // loop over synapses/source neurons
			if (synapseTypeList[i][j] != 4){
				caerDynapseWriteCam(state->eventSourceModuleState, inputAddrList[i][j], neuronAddr[i], j, synapseTypeList[i][j]);
			}
		}
	}
	caerLog(CAER_LOG_NOTICE, __func__, "CAM programmed successfully.");
}

void writeTestConnections(optimizerState state){
	caerLog(CAER_LOG_NOTICE, __func__, "Started programming CAM for test input stimulus");

#if CRICKET_NETWORK_1

	uint16_t neuronToStim = 256;

	for (uint16_t neuronId = 1*256; neuronId < 1*256+neuronToStim; neuronId++) { // loop over receiving neurons
		caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[inLF_L], neuronId, 1, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		caerDynapseWriteCam(state->eventSourceModuleState, neuronAddr[inLF_L], neuronId, 2, DYNAPSE_CONFIG_CAMTYPE_F_INH);
	}
#endif CRICKET_NETWORK_1

	caerLog(CAER_LOG_NOTICE, __func__, "CAM programmed for test connections successfully.");
}

void writePIRConnections(optimizerState state)
{
	caerLog(CAER_LOG_NOTICE, __func__, "Started programming CAM for PIR input stimulus.");

	uint8_t coreId = coreId_LN3;
	uint16_t neuronsToStim = 256;

	// loop over receiving neurons
	for (uint16_t neuronId = coreId*256; neuronId < (coreId*256 + neuronsToStim); neuronId++)
	{
		// delay element #1
		caerDynapseWriteCam(state->eventSourceModuleState, 1, neuronId, 0, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		caerDynapseWriteCam(state->eventSourceModuleState, 1, neuronId, 1, DYNAPSE_CONFIG_CAMTYPE_F_INH);

		// delay element #2
		//caerDynapseWriteCam(state->eventSourceModuleState, 2, neuronId, 2, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		//caerDynapseWriteCam(state->eventSourceModuleState, 2, neuronId, 3, DYNAPSE_CONFIG_CAMTYPE_F_INH);

		// delay element #3
		//caerDynapseWriteCam(state->eventSourceModuleState, 3, neuronId, 4, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		//caerDynapseWriteCam(state->eventSourceModuleState, 3, neuronId, 5, DYNAPSE_CONFIG_CAMTYPE_F_INH);
	}

	caerLog(CAER_LOG_NOTICE, __func__, "CAM programmed for PIR connections successfully.");
}

void changePirCams(optimizerState state)
{
	//caerLog(CAER_LOG_NOTICE, __func__, "Started changing CAMs for PIR input stimulus");

	uint8_t coreId = coreId_LN3;
	uint16_t neuronId = coreId*256 + 22;

	// clear cams
	caerDynapseWriteCam(state->eventSourceModuleState, 0, neuronId, camId_PIR_sExc, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
	caerDynapseWriteCam(state->eventSourceModuleState, 0, neuronId, camId_PIR_fInh, DYNAPSE_CONFIG_CAMTYPE_F_INH);

	// increase CAM IDs
	camId_PIR_sExc++;
	camId_PIR_fInh++;

	camIteration++;

	// check wether at last CAM
	if (camId_PIR_sExc > 63)
	{
		camId_PIR_sExc = 0;
		camEpoch++;
	}

	if (camId_PIR_fInh > 63)
	{
		camId_PIR_fInh = camEpoch;
	}

	caerDynapseWriteCam(state->eventSourceModuleState, 1, neuronId, camId_PIR_sExc, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
	caerDynapseWriteCam(state->eventSourceModuleState, 1, neuronId, camId_PIR_fInh, DYNAPSE_CONFIG_CAMTYPE_F_INH);

	printf("%d:\t cam_sExc = %d, cam_fInh = %d.\n", camIteration, camId_PIR_sExc, camId_PIR_fInh);

	//caerLog(CAER_LOG_NOTICE, __func__, "CAMs successfully changed for PIR connections.");
}

void writePolyPairConnections(optimizerState state)
{
	caerLog(CAER_LOG_NOTICE, __func__, "Started programming CAM for polychronous input stimulus");

	uint8_t coreId = coreId_LN3;
	uint16_t neuronsToStim = 256;

	// loop over receiving neurons
	for (uint16_t neuronId = coreId*256; neuronId < (coreId*256 + neuronsToStim); neuronId++)
	{
		caerDynapseWriteCam(state->eventSourceModuleState, 1, neuronId, 0, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		caerDynapseWriteCam(state->eventSourceModuleState, 1, neuronId, 1, DYNAPSE_CONFIG_CAMTYPE_F_INH);

		caerDynapseWriteCam(state->eventSourceModuleState, 2, neuronId, 2, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		caerDynapseWriteCam(state->eventSourceModuleState, 2, neuronId, 3, DYNAPSE_CONFIG_CAMTYPE_F_INH);

		//caerDynapseWriteCam(state->eventSourceModuleState, 3, neuronId, 4, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		//caerDynapseWriteCam(state->eventSourceModuleState, 3, neuronId, 5, DYNAPSE_CONFIG_CAMTYPE_F_INH);
	}

	caerLog(CAER_LOG_NOTICE, __func__, "CAM programmed for pair connections successfully.");
}

void writePolyTripletConnections(optimizerState state)
{
	caerLog(CAER_LOG_NOTICE, __func__, "Started programming CAMs for triplet input stimulus");

	uint8_t coreId = coreId_LN3;
	uint16_t neuronsToStim = 256;

	// loop over receiving neurons
	for (uint16_t neuronId = coreId*256; neuronId < (coreId*256 + neuronsToStim); neuronId++)
	{
		// inhibition
		caerDynapseWriteCam(state->eventSourceModuleState, 1, neuronId, 0, DYNAPSE_CONFIG_CAMTYPE_F_INH);

		// excitation
		caerDynapseWriteCam(state->eventSourceModuleState, 2, neuronId, 1, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		caerDynapseWriteCam(state->eventSourceModuleState, 3, neuronId, 2, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
		caerDynapseWriteCam(state->eventSourceModuleState, 4, neuronId, 3, DYNAPSE_CONFIG_CAMTYPE_S_EXC);
	}

	caerLog(CAER_LOG_NOTICE, __func__, "CAMs programmed for triplet connections successfully.");
}

void writeChipSram(optimizerState state){
	uint8_t sramId = 1; // (SRAM 0 is used for USB monitoring)

	for (uint16_t neuronId=1*256; neuronId<4*256; neuronId++){ // loop over neurons - cores 1-3
		//uint16_t neuronId = neuronAddr[i]; // get neuron address
		uint8_t coreId = neuronId >> 8; // get neuron core ID
		//uint8_t localNeuronId = neuronId % 256; // get local neuron ID on core

		// send to the same chip
		uint8_t dx = 0;
		bool sx = 0;
		uint8_t dy = 0;
		bool sy = 0;

		uint8_t virtualCoreId = coreId;
		uint8_t destinationCore = 15; // send to all cores (hot-coded: 15 -> binary number 1111)

		// send data via usb (note that chip id has been selected previously)
		bool writeSRAM = caerDynapseWriteSramN(state->eventSourceModuleState, neuronId, sramId, virtualCoreId, sx, dx, sy, dy, destinationCore);
		if (!writeSRAM){
			caerLog(CAER_LOG_NOTICE, __func__, "Failed programming on-chip SRAM.");
		}
	}
}


// MONTE CARLO FUNCTIONS

void mcExchangeCam(optimizerState state){ // perform Monte Carlo exchange of input addresses between two CAM cells

	// Exchange synapse addresses
	inputAddr1 = inputAddrList[mcPos][camId2];
	inputAddr2 = inputAddrList[mcPos][camId1];

	// Update list of synapse addresses
	inputAddrList[mcPos][camId1] = inputAddr1;
	inputAddrList[mcPos][camId2] = inputAddr2;

	// Exchange synapse types
	synType1 = synapseTypeList[mcPos][camId2];
	synType2 = synapseTypeList[mcPos][camId1];

	// Update list of synapse types
	synapseTypeList[mcPos][camId1] = synType1;
	synapseTypeList[mcPos][camId2] = synType2;

	// Perform change
	//bool caerDynapseWriteCam(caerDeviceHandle handle, uint16_t inputNeuronAddr, uint16_t neuronAddr, uint8_t camId, uint8_t synapseType);

	// first synapse (CAM-cell 1)
	bool writeCam1;
	if (synType1 == 4){ // no connection/synapse
		writeCam1 = caerDynapseWriteCam(state->eventSourceModuleState, 0, mcPos, camId1, 0);
	}
	else{
		writeCam1 = caerDynapseWriteCam(state->eventSourceModuleState, inputAddr1, mcPos, camId1, synType1);
	}

	if (!writeCam1){
		caerLog(CAER_LOG_NOTICE, __func__, "Failed programming CAM 1.");
	}

	// second synsapse (CAM-cell 2)
	bool writeCam2;
	if (synType2 == 4){ // no connection/synapse
		writeCam2 = caerDynapseWriteCam(state->eventSourceModuleState, 0, mcPos, camId2, 0);
	}
	else{
		writeCam2 = caerDynapseWriteCam(state->eventSourceModuleState, inputAddr2, mcPos, camId2, synType2);
	}

	if (!writeCam2){
		caerLog(CAER_LOG_NOTICE, __func__, "Failed programming CAM 2.");
	}
}

void mcChangeNeuronAddr(optimizerState state){
	caerLog(CAER_LOG_NOTICE, __func__, "mcChangeNeuronAddr called.");

	bool duplicate; // duplicate detection indicator
	int randStartInd = numInput;

	//int seed = time(NULL); // get seed from system clock
	//randomise(seed);

	lastNeuronAddr = neuronAddr[mcPos]; // store neuron address
	neuronAddr[mcPos] = randAddrGen(mcPos);

	// avoid creating duplicates
	duplicate = true;
	// check for duplicates and generate new address until neuron address i is unique - not for i=0
	while (duplicate){
		// loop over previously generated addresses
		for (int i=randStartInd; i<numNeur; i++){
			// check for duplicate - but not with itself
			if ((i != mcPos) && (neuronAddr[mcPos] == neuronAddr[i])){
				// generate new address at mcPos - middle/motor neurons
				neuronAddr[mcPos] = randAddrGen(mcPos);
				break; // terminate loop over assigned addresses - while-loop continues
			}
		}
		duplicate = false; // no duplicate was found for address at mcPos
	}
	//printf("lastNeuronAddr = %d, neuronAddr[%d] = %d", lastNeuronAddr, mcPos, neuronAddr[mcPos]);

	// clear CAM of last neuron address, and program CAM of new address
	for (uint8_t j=0; j<numSyn; j++){ // loop over synapses/source neurons
		//caerDynapseWriteCam(state->eventSourceModuleState, 0, neuronAddr[mcPos], j, 0); // clear CAM of new neuron - for preparation
		if (synapseTypeList[mcPos][j] != 4){
			caerDynapseWriteCam(state->eventSourceModuleState, 0, lastNeuronAddr, j, 0); // clear CAM of old neuron
			caerDynapseWriteCam(state->eventSourceModuleState, inputAddrList[mcPos][j], neuronAddr[mcPos], j, synapseTypeList[mcPos][j]); // write CAM of new neuron
		}
	}

	// rewrite CAMs of neurons receiving from the one being changed
	for (uint16_t i=numInput; i<numNeur; i++){
		for (uint8_t j=0; j<numSyn; j++){
			if (inputAddrList[i][j] == lastNeuronAddr){
				inputAddrList[i][j] = neuronAddr[mcPos];
				caerDynapseWriteCam(state->eventSourceModuleState, inputAddrList[i][j], neuronAddr[i], j, synapseTypeList[i][j]); // write CAM of new neuron
			}
		}
	}
	printf("Changed neuron %s from %d to %d.\n", neuronNames[mcPos], lastNeuronAddr, neuronAddr[mcPos]);
	//caerLog(CAER_LOG_NOTICE, __func__, "mcChangeNeuronAddr executed.");
}

void mcResetNeuronAddr(optimizerState state){
	caerLog(CAER_LOG_NOTICE, __func__, "mcResetNeuronAddr called.");

	// clear CAMs of tested address; rewrite CAM of last address
	for (uint8_t j=0; j<numSyn; j++){ // loop over synapses (CAMs)
		if (synapseTypeList[mcPos][j] != 4){
			caerDynapseWriteCam(state->eventSourceModuleState, 0, neuronAddr[mcPos], j, 0); // clear CAM of tried neuron
			caerDynapseWriteCam(state->eventSourceModuleState, inputAddrList[mcPos][j], lastNeuronAddr, j, synapseTypeList[mcPos][j]); // write CAM of last neuron
		}
	}

	// rewrite CAMs of neurons receiving from the one being changed
	for (uint16_t i=numInput; i<numNeur; i++){
		for (uint8_t j=0; j<numSyn; j++){
			if (inputAddrList[i][j] == neuronAddr[mcPos]){
				inputAddrList[i][j] = lastNeuronAddr;
				caerDynapseWriteCam(state->eventSourceModuleState, inputAddrList[i][j], neuronAddr[i], j, synapseTypeList[i][j]); // write CAM of new neuron
			}
		}
	}
	neuronAddr[mcPos] = lastNeuronAddr; // reset neuron address
	//caerLog(CAER_LOG_NOTICE, __func__, "mcResetNeuronAddr executed.");
}

void mcInit(optimizerState state){ // initiate new Monte Carlo test exchange
	//caerLog(CAER_LOG_NOTICE, __func__, "mcInit called.");

	//printf("Started mcInit, mcPos = %d, stimCycles = %d\n", mcPos, stimCycles);

	// initialize random number generator
	//int seed = time(NULL); // get seed from system clock
	//randomise(seed);

	//mcExchangeCam(state); // perform Monte Carlo exchange
	mcChangeNeuronAddr(state);
	mcTest = true; // simulation is now in state of exchange test
	//caerLog(CAER_LOG_NOTICE, __func__, "mcInit executed.");
}

void mcEvaluate(optimizerState state){ // validate current Monte Carlo exchange
	//caerLog(CAER_LOG_NOTICE, __func__, "mcEvaluate called.");

	printf("\nsweepInd = %d.\n", sweepInd);

	// minimize cost function
	if (incrTargetDiffSum){ // no improvement - revert change
		mcResetNeuronAddr(state); // revert Monte Carlo sample
		printf("incrTargetDiffSum = %d, changing back targetDiffSum from %f to %f.\n", incrTargetDiffSum, targetDiffSum, lastTargetDiffSum);
		targetDiffSum = lastTargetDiffSum; // reset loss function value
		changeKept = 0;
		printf("%s: Monte Carlo exchange reverted.\n", neuronNames[mcPos]);
	}
	else{
		changeKept = 1;
		printf("incrTargetDiffSum = %d, targetDiffSum changed from %f to %f\n", incrTargetDiffSum, lastTargetDiffSum, targetDiffSum);
		printf("%s: Monte Carlo exchange kept.\n", neuronNames[mcPos]);
	}
	/*// print generated random neuron addresses
	for (int i=0; i<numNeur; i++){
		printf("neuronAddr %d = %d.\n", i, neuronAddr[i]);
	}*/

	mcTest = false; // exchange test finished

	mcPos++;
	if (mcPos >= numNeur){ // check if sweep over all neurons is finished
		mcPos = numInput; // Start over at first neuron - input neurons not included
		sweepInd++; // Update sweep index
	}
	//caerLog(CAER_LOG_NOTICE, __func__, "mcEvaluate executed.");
}

void mcObjectiveFunction(){
	//caerLog(CAER_LOG_NOTICE, __func__, "mcObjectiveFunction called.");

	lastTargetDiffSum = targetDiffSum;

#if CRICKET_NETWORK_1
	//countDiffSum = countDiff_L + countDiff_R;
	targetDiffSum = targetDiff_L + targetDiff_R;
#endif // CRICKET_NETWORK_1

#if CRICKET_NETWORK_2
	targetDiffSum = targetDiff_singlePulse + targetDiff_doublePulse;
#endif // CRICKET_NETWORK_2

	incrTargetDiffSum = (lastTargetDiffSum <= targetDiffSum);
	//printf("lastTargetDiffSum = %f, targetDiffSum = %f, incrTargetDiffSum = %d.\n", lastTargetDiffSum, targetDiffSum, incrTargetDiffSum);
}


// BIASING FUNCTIONS

void silentBiases(optimizerState state){
	for (uint8_t chipId = 0; chipId < 4; chipId++){
		for (uint8_t coreId = 0; coreId < 4; coreId++){
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 0, false); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 7, 0, false); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 7, 0, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 7, 0, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 0, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 7, 0, true); //
		}
	}
	caerLog(CAER_LOG_NOTICE, __func__, "silentBiases executed.");
}

void softSpikeGenBiases(optimizerState state){
	for (uint8_t chipId = 0; chipId < 1; chipId++){
		for (uint8_t coreId = 1; coreId < 4; coreId++) {
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 5, 2, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 7, 1, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 2, 180, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 4, 225, false); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 4, 225, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 2, 180, true); // (!)

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 6, 150, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 40, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 0, 200, true); // (1)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 7, 0, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 7, 40, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 7, 40, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true);

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 250, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 7, 1, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 7, 1, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true);

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 3, 50, true);
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true);
		}
	}
	caerLog(CAER_LOG_NOTICE, __func__, "softSpikeGenBiases set.");
}

void lowPowerBiases(optimizerState state){
	for (uint8_t chipId = 3; chipId < 4; chipId++){
		for (uint8_t coreId = 3; coreId < 4; coreId++) {
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 1, 30, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 3, 3, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 7, 5, false); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 6, 100, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 4, 120, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 7, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 7, 0, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 3, 106, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //
		}
	}
	caerLog(CAER_LOG_NOTICE, __func__, "lowPowerBiases set.");
}

void workingBiases(optimizerState state){
	for (uint8_t chipId = 0; chipId < 1; chipId++){
		for (uint8_t coreId = 1; coreId < 4; coreId++) {
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 7, 2, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 208, true); // (0, 108)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 6, 21, false); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 5, 15, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 3, 20, true); // (!)

			// fast excitatory
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 53, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 1, 100, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 250, true); // (!)

			// slow excitatory
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 7, 0, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 7, 1, true); //

			// fast inhibitory
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 7, 200, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 69, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, coreId*85, true); //

			// slow inhibitory
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 1, true); // (!)

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 0, 43, true); // (0, 43), (3, 106)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //
		}
	}
	caerLog(CAER_LOG_NOTICE, __func__, "workingBiases set.");
}

void customNicolettaBiases(optimizerState state){
	for (uint8_t chipId = 0; chipId < 1; chipId++){
		for (uint8_t coreId = 1; coreId < 4; coreId++) {
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 7, 0, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 7, 5, false); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 3, 50, true); // (!)

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 100, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 5, 91, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 220, true); // (!)

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 5, 5, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 5, 5, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 0, 255, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 5, 41, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 0, 150, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, 50*coreId, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 5, 41, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 0, 150, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 0, 114, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 4, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //
		}
	}
	caerLog(CAER_LOG_NOTICE, __func__, "customNicolettaBiases set.");
}

void nicolettaBiases(optimizerState state){  // From Nicoletta Risi and Giacomo Indiveri, tuned to give 50-100 ms timescale (of synapses)
	for (uint8_t chipId = 0; chipId < 1; chipId++){
		for (uint8_t coreId = 1; coreId < 4; coreId++) {
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 7, 0, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 4, true); // (!)

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 100, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 5, 5, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 5, 91, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 5, 5, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 5, 41, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 5, 41, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 0, 150, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 0, 150, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 33, true); // (!)
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 0, 255, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, 100, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 0, 114, true); //

			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 4, 40, true); //
			caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //
		}
	}
	caerLog(CAER_LOG_NOTICE, __func__, "nicolettaBiases set.");
}


// FEATURE DETECTION AND PIR BIAS FUNCTIONS

void setBiases_LN2(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN2;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 7, 2, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 208, true); // (0, 108)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 6, 21, false); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 5, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 3, 20, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 165, true); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 1, 100, true); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 190, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 1, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 0, 43, true); // (0, 43), (3, 106)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "LN2 biases set.");
}

void setBiases_LN3(optimizerState state){
	// LN3 neuron with integrated LN5 PIR functionality by synapses

	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN3;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 0, 40, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 4, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 200, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 6, 120, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 1, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 1, 30, true); // (retuned)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 5, 100, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 60, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 1, 150, true); // (!)
	//caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 120, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 1, 110, true); // (kept)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 1, 130, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 5, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "LN3 biases set.");
}

void setBiases_LN4(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN4;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 7, 2, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 208, true); // (0, 108)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 6, 21, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 5, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 3, 20, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 80, true); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 1, 140, true); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 6, 180, true); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 140, true); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 76, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, 60, true); // (tuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 1, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 0, 43, true); // (0, 43), (3, 106)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "LN4 biases set.");
}

void setBiases_LN3_100ms(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN3;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 1, 50, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 4, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 6, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 100, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 2, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 3, 30, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 6, 100, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 150, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 0, 100, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, 100, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 5, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "LN3 biases set.");
}

void setBiases_PIR(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN3;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 0, 40, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 4, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 125, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 6, 120, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 2, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 1, 30, true); // (retuned)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 5, 100, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 60, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
	//caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 120, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 1, 110, true); // (kept)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 1, 130, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 5, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "PIR biases set.");
}

void setBiases_PIR_100ms(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN3;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 0, 40, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 4, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 125, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 190, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 2, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 1, 30, true); // (retuned)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 6, 40, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 60, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
	//caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 120, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 1, 70, true); // (kept)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 1, 50, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 5, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "PIR biases set.");
}

void setBiases_PIR_100ms_bottom(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN3;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 1, 30, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 4, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 125, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 7, 210, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 2, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 1, 30, true); // (retuned)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 6, 80, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 60, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
	//caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 120, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 0, 60, true); // (kept)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, 150, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 5, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "PIR biases set.");
}

void setBiases_polyPair(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN3;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 1, 30, true); // (retuned)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
	
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 135, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 125, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 5, 70, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 2, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 0, 210, true); // (retuned)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 5, 100, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 60, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
	//caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 120, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 0, 140, true); // (0, 230)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, 150, true); // (0, 150)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 5, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "Poly biases set.");
}

void setBiases_polyTriplet(optimizerState state){
	uint8_t chipId = 0;
	uint8_t coreId = coreId_LN3;

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTAU_N", 7, 35, false); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHTHR_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_AHW_P", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_BUF_P", 3, 80, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_CASC_N", 7, 1, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_DC_P", 1, 30, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_NMDA_N", 1, 213, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_RFR_N", 4, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU1_N", 5, 39, false); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_TAU2_N", 0, 15, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "IF_THR_N", 6, 135, true); // (!)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_F_P", 5, 125, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_TAU_S_P", 5, 70, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_F_P", 2, 30, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPIE_THR_S_P", 0, 210, true); // (retuned)

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_F_P", 5, 100, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_TAU_S_P", 7, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_F_P", 3, 60, true); // (retuned)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "NPDPII_THR_S_P", 7, 40, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 7, 0, true); // (!)
	//caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_F_N", 0, 120, true); // (!)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_EXC_S_N", 3, 130, true); // (3, 130)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_F_N", 0, 150, true); // (0, 150)
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PS_WEIGHT_INH_S_N", 7, 0, true); //

	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "PULSE_PWLK_P", 5, 40, true); //
	caerDynapseSetBiasCore(state->eventSourceConfigNode, chipId, coreId, "R2R_P", 4, 85, true); //

	caerLog(CAER_LOG_NOTICE, __func__, "Triplet biases set.");
}


// STIMULATION FUNCTIONS

void runStimVariableIsi(optimizerState state, uint32_t lines){
	//caerLog(CAER_LOG_NOTICE, __func__, "runStimVariableIsi called.");
	//printf("stim_singlePulse = %d, stim_doublePulse = %d.\n", stim_singlePulse, stim_doublePulse);

	uint32_t stimCountVar = lines - 1;

	// Start/Stop Stimulation. It will finish a complete stimulation before ending
	runVar = true;
	int retval;
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN, isiFactorVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "ISI failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_ISI, isiFactorVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "ISI failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_ISIBASE, isiBaseVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "ISI base failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_VARMODE, varModeVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "varMode failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_BASEADDR, baseAddrVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "Base address failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_STIMCOUNT, stimCountVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "stimCount failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_REPEAT, repeatVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "Repeat failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_RUN, runVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "run status failed to update");
	}
	else if (retval == 1) {
		//caerLog(CAER_LOG_NOTICE, __func__, "Run status updated successfully.");
	}
}

void runStimFixedIsi(optimizerState state, uint16_t *spikeTrain){
	caerLog(CAER_LOG_NOTICE, __func__, "runStimFixedIsi called.");

	caerDynapseWriteSramWords(state->eventSourceModuleState, (uint16_t *)spikeTrain, baseAddrFixed, stimCountFixed); // write FPGA SRAM
	caerLog(CAER_LOG_NOTICE, __func__, "FPGA SRAM programmed.");

	// Start/Stop Stimulation. It will finish a complete stimulation before ending
	runFixed = true;
	int retval;
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN, isiFactorFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "ISI failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_ISI, isiFactorFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "ISI failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_ISIBASE, isiBaseFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "ISI base failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_VARMODE, varModeFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "varMode failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_BASEADDR, baseAddrFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "Base address failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_STIMCOUNT, stimCountFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "stimCount failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_REPEAT, repeatFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "Repeat failed to update");
	}
	retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_RUN, runFixed);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "run status failed to update");
	}
	else if (retval == 1){
		caerLog(CAER_LOG_NOTICE, __func__, "run status succeeded to update");
	}
	caerLog(CAER_LOG_NOTICE, __func__, "runStimFixedIsi executed.");
}

void startStereoStim(optimizerState state, uint16_t *spikeTrain_L, uint16_t *spikeTrain_R){ // start new stereo stimulus cycle

	//uint32_t stimCount_L;
	//uint32_t stimCount_R;

	// change incidence direction of simulus
	if (!stim_L){
#if GENERATE_SPIKES
		if (!genTrain_L){
			stimCount_L = genPoissonSpikeTrain(spikeTrain_L, neuronAddr[inLF_L], neuronAddr[inLF_R], firingRateLF_high, firingRateLF_low); // left-incident signal
			printSpikeTrain(spikeTrain_L, stimCount_L, "poissonTrain_L.txt");
#if CLONE_POISSON
			genTrain_L = true; // stop generating new Poisson spike trains
#endif // CLONE_POISSON
		}
		//#endif // GENERATE_SPIKES
#elif SPIKES_FROM_FILE
		if (!readTrain_L){
			stimCount_L = variableIsiFileToSpikeTrain(spikeTrain_L, "poissonTrain_L.txt");
			readTrain_L = true;
		}
#endif // SPIKES_FROM_FILE

		size_t numWords_L = 2 * stimCount_L;
		caerDynapseWriteSramWords(state->eventSourceModuleState, (uint16_t *)spikeTrain_L, baseAddrVar, numWords_L); // write FPGA SRAM
		caerLog(CAER_LOG_NOTICE, __func__, "FPGA SRAM programmed for left-incident stimulus.");
		runStimVariableIsi(state, stimCount_L); // run left-incident stimulus
		stim_R = false;
		stim_L = true;
	}
	else if (!stim_R){
#if GENERATE_SPIKES
		if (!genTrain_R){
			stimCount_R = genPoissonSpikeTrain(spikeTrain_R, neuronAddr[inLF_R], neuronAddr[inLF_L], firingRateLF_high, firingRateLF_low); // right-incident signal
			printSpikeTrain(spikeTrain_R, stimCount_R, "poissonTrain_R.txt");
#if CLONE_POISSON
			genTrain_R = true; // stop generating new Poisson spike trains
#endif // CLONE_POISSON
		}
		//#endif // GENERATE_SPIKES
#elif SPIKES_FROM_FILE
		if (!readTrain_R){
			stimCount_R = variableIsiFileToSpikeTrain(spikeTrain_R, "poissonTrain_R.txt");
			readTrain_R = true;
		}
#endif // SPIKES_FROM_FILE
		size_t numWords_R = 2 * stimCount_R;
		caerDynapseWriteSramWords(state->eventSourceModuleState, (uint16_t *)spikeTrain_R, baseAddrVar, numWords_R); // write FPGA SRAM
		caerLog(CAER_LOG_NOTICE, __func__, "FPGA SRAM programmed for right-incident stimulus.");
		runStimVariableIsi(state, stimCount_R); // run right-incident stimulus
		stim_L = false;
		stim_R = true;
	}
	//caerLog(CAER_LOG_NOTICE, __func__, "Stimulus changed.");
	//stimCycles = 0; // reset stimulus cycles counter
	startedStim = true;

	// reset stimulus clock
	struct timespec stimStart;
	portable_clock_gettime_monotonic(&stimStart);
	stimStartedAt = (double) stimStart.tv_sec + 1.0e-9 * (double) stimStart.tv_nsec;
}

void startPulseStim(optimizerState state, uint16_t *spikeTrain_1, uint16_t *spikeTrain_2){ // start new stereo stimulus cycle
	//caerLog(CAER_LOG_NOTICE, __func__, "startPulseStim called.");

	// change incidence direction of simulus
	if (!stim_singlePulse)
	{
		//caerLog(CAER_LOG_NOTICE, __func__, "Running single-pulse stimulus.");
		if (!genTrain_1)
		{
#if IND_PULSE_GEN || IND_PIR_GEN
			// generate x single pulse-pairs of each interval from array
			//stimCount_1 = genPulsePair(spikeTrain_1, neuronAddr[AN1], 0, pulseIntervals[ipiIndex]);
			stimCount_1 = genSingleSpike(spikeTrain_1, neuronAddr[AN1]);
			//stimCount_1 = genPolyPair(spikeTrain_1, pairISIs[ipiIndex]);
			//stimCount_1 = genPolyTriplet(spikeTrain_1, tripletISIs[ipiIndex]);
			pulsePairCounter++;

			// check whether N=100 is reached for current ISI
			if (pulsePairCounter % 100 == 0)
			{
				ipiIndex++;

				// check if end of ISI list is reached
				if (ipiIndex >= 11)
				{
					ipiIndex = 0;

					// update weight
					wIndex++;
					if (wIndex >= numWeights)
						wIndex = 0;
					//caerDynapseSetBiasCore(state->eventSourceConfigNode, 0, coreId_LN3, "PS_WEIGHT_INH_F_N", 0, wInh[wIndex], true);
					//caerDynapseSetBiasCore(state->eventSourceConfigNode, 0, coreId_LN3, "PS_WEIGHT_EXC_S_N", 0, wExc[wIndex], true);

					//printf("wInh = %d\n", wInh[wIndex]);
					//printf("wExc = %d\n", wExc[wIndex]);

				}
				printf("ipiIndex = %d\n", ipiIndex);
			}
#else
			//stimCount_1 = genPulseSpikeTrain(spikeTrain_1, neuronAddr[AN1], 0); // single pulse signal
			//stimCount_1 = genSingleSpike(spikeTrain_1, neuronAddr[AN1]);
			//stimCount_1 = genPolyPairTrain(spikeTrain_1);
			stimCount_1 = genPolyTripletTrain(spikeTrain_1);
#endif // IND_PULSE_GEN

			//printf("stimCount_1 = %d.\n", stimCount_1);
			printSpikeTrain(spikeTrain_1, stimCount_1, "pulseTrain_single.txt");
			//genTrain_1 = true; // stop generating new pulse spike trains
			//caerLog(CAER_LOG_NOTICE, __func__, "Generated single-pulse spike-train.");
		}
		size_t numWords_1 = 2 * stimCount_1;
		bool retval = caerDynapseWriteSramWords(state->eventSourceModuleState, (uint16_t *)spikeTrain_1, baseAddrVar, numWords_1); // write FPGA SRAM
		if (!retval)
			printf("Failed programming single-pulse stimulus to FPGA SRAM, baseAddr = %d\n.\n", baseAddrVar);
		runStimVariableIsi(state, stimCount_1);
#if FIRST_SPKTRN_ONLY

#else
		stim_singlePulse = true;
		stim_doublePulse = false;

		monitoring = true; // keep monitoring for subsequent double pulse
#endif // PIR_PULSE_STIM
	}
	else if (!stim_doublePulse)
	{
		//caerLog(CAER_LOG_NOTICE, __func__, "Running double-pulse stimulus.");
		if (!genTrain_2)
		{
			stimCount_2 = genPulseSpikeTrain(spikeTrain_2, neuronAddr[AN1], 1); // double pulse signal
			//printf("stimCount_2 = %d.\n", stimCount_2);
			printSpikeTrain(spikeTrain_2, stimCount_2, "pulseTrain_double.txt");
			genTrain_2 = true; // stop generating new pulse spike trains
			caerLog(CAER_LOG_NOTICE, __func__, "Generated double-pulse spike-train.");
		}
		size_t numWords_2 = 2 * stimCount_2;
		bool retval = caerDynapseWriteSramWords(state->eventSourceModuleState, (uint16_t *)spikeTrain_2, baseAddrVar, numWords_2); // write FPGA SRAM
		if (!retval)
			printf("Failed programming double-pulse stimulus to FPGA SRAM, baseAddr = %d.\n", baseAddrVar);
		runStimVariableIsi(state, stimCount_2);
		stim_singlePulse = false;
		stim_doublePulse = true;
	}
	//caerLog(CAER_LOG_NOTICE, __func__, "Stimulus changed.");
	//stimCycles = 0; // reset stimulus cycles counter
	startedStim = true;

	// reset stimulus clock
	struct timespec stimStart;
	portable_clock_gettime_monotonic(&stimStart);
	stimStartedAt = (double) stimStart.tv_sec + 1.0e-9 * (double) stimStart.tv_nsec;
}

void endStereoStim(optimizerState state){ // end current stereo stimulus cycle

	// stop spike generation
	runVar = false; // Start/Stop Stimulation. It will finish a complete stimulation before ending
	int retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_RUN, runVar);
	if (retval == 0)
		caerLog(CAER_LOG_NOTICE, __func__, "Run status failed to update.");
	else if (retval == 1)
		caerLog(CAER_LOG_NOTICE, __func__, "Run status updated successfully.");
	//caerLog(CAER_LOG_NOTICE, __func__, "Stimulus cycle ended.");
	/*printf("\nstim_L = %d, stim_R = %d\n"
			"lastMeanRate_L = %f, meanRate_L = %f\n"
			"lastMeanRate_R = %f meanRate_R = %f.\n\n",
			stim_L, stim_R, lastMeanRate_L, meanRate_L, lastMeanRate_R, meanRate_R);*/
	/*if (stim_L){
		printf("\nstim_L.\n");
	}
	else if (stim_R){
		printf("\nstim_R.\n");
	}
	printf("spikeCount_L = %d, spikeCount_R = %d\n\n", spikeCount_L, spikeCount_R);*/
	startedStim = false;
	stimRuns++;
}

void endPulseStim(optimizerState state){ // end current stereo stimulus cycle
	//caerLog(CAER_LOG_NOTICE, __func__, "endPulseStim called.");
	//printf("stim_singlePulse = %d, stim_doublePulse = %d.\n", stim_singlePulse, stim_doublePulse);

	// THIS DOES NOT WORK
	// stop spike generation
	/*runVar = false; // Start/Stop Stimulation. It will finish a complete stimulation before ending
	int retval = caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_SPIKEGEN, DYNAPSE_CONFIG_SPIKEGEN_RUN, runVar);
	if ( retval == 0 ) {
		caerLog(CAER_LOG_NOTICE, __func__, "run status failed to update");
	}
	else if (retval == 1) {
		caerLog(CAER_LOG_NOTICE, __func__, "Pulse stimulus ended successfully.");
	}*/

	startedStim = false;
	stimRuns++;
	//printf("stimRuns = %d.\n", stimRuns);
}


// STIMULUS GENERATION FUNCTIONS

uint32_t genPoissonSpikeTrain(uint16_t *spikeTrain, uint16_t inAddr_adj, uint16_t inAddr_opp, uint8_t r_adj, uint8_t r_opp){
	//caerLog(CAER_LOG_NOTICE, __func__, "Started generating Poisson spike train.");

	// variables
	double varIsi = dt; // variable inter-spike interval [us] - min. value is one time step
	double x_adj; // random number for probability test
	double x_opp;
	double t = dt; // time [s]
	uint32_t poissonEventCount = 0;
	uint32_t varStimCount; // variable stimulation event counter

	uint16_t fpgaAddr_adj = (inAddr_adj & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4; // bitfield for input adjacent to direction of incidence
	uint16_t fpgaAddr_opp = (inAddr_opp & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4; // bitfield for input opposite to direction of incidence
	//printf("fpgaAddr_adj = %d, fpgaAddr_opp = %d.\n", fpgaAddr_adj, fpgaAddr_opp);

	// initialize random number generator
	//int seed = time(NULL); // get seed from system clock
	//randomise(seed);

	// generate Poisson distributed spike events for low freq. stim.
	while (t <= stimTime){

		// control against probability for spike events - probability of spike during dt = r*dt
		x_adj = ranf(); // [0,1)
		x_opp = ranf(); // [0,1)
		if ((x_adj <= r_adj*dt) && (x_opp <= r_opp*dt)){ // simultaneous events

			// adjacent side receptor cell event
			spikeTrain[2*poissonEventCount] = fpgaAddr_adj;
			spikeTrain[2*poissonEventCount + 1] = 0; // isi = isiFactor * (isiBase/90) us | compensate for stereo delay | can it be zero?
			poissonEventCount++; // add event to counter

			// opposite side receptor cell event
			spikeTrain[2*poissonEventCount] = fpgaAddr_opp;
			spikeTrain[2*poissonEventCount + 1] = (varIsi)/(isiBaseVar/90e6); // isiFactor |isi = isiFactor * (isiBase/90) us
			poissonEventCount++; // add event to counter
			varIsi = dt; // reset ISI variable to min. value
		}
		else if (x_adj <= r_adj*dt){ // adjacent side receptor cell event
			spikeTrain[2*poissonEventCount] = fpgaAddr_adj;
			spikeTrain[2*poissonEventCount + 1] = (varIsi)/(isiBaseVar/90e6); // isi = isiFactor * (isiBase/90) us
			poissonEventCount++; // add event to counter
			varIsi = dt; // reset ISI variable to min. value
		}
		else if (x_opp <= r_opp*dt){ // opposite side receptor cell spike event
			spikeTrain[2*poissonEventCount] = fpgaAddr_opp;
			spikeTrain[2*poissonEventCount + 1] = (varIsi)/(isiBaseVar/90e6); // isi = isiFactor * (isiBase/90) us
			poissonEventCount++; // add event to counter
			varIsi = dt; // reset ISI variable to min. value
		}
		else{
			varIsi += dt; // no spike yet - increase ISI variable by one time step
		}
		t += dt; // update time
	}
	//printf("poissonEventCount = %d.\n", poissonEventCount);
	varStimCount = poissonEventCount; // number of lines for FPGA stimulus
	//caerLog(CAER_LOG_NOTICE, __func__, "Poisson stereo spike train generated.");
	return(varStimCount);
}

uint32_t genPulseSpikeTrain(uint16_t *spikeTrain, uint16_t inputAddr, bool doublePulse){
	//caerLog(CAER_LOG_NOTICE, __func__, "Started generating Poisson spike train.");
	//int16_t clockFreq = dynapseInfo.logicClock; // [MHz]
	//printf("clockFreq = %d MHz./n", clockFreq);
	uint32_t eventCount = 0;
	uint16_t pulseIsi = (1/(float)pulseFiringRate) / (isiBaseVar/((float)clockFreq*1e6));
	//uint16_t pulseIsi = 2;

	float perturbFactor;
	//float perturbationTimeConst = e-3 / (isiBaseVar/(clockFreq*1e6));

	float stimulationInterval = 250e-3; // [s]
	float IPIs[] = {1/(float)pulseFiringRate, 10e-3, 20e-3, 30e-3, 40e-3, 50e-3}; // [s]

	uint16_t fpgaInputAddr = (inputAddr & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// single pulse
	for (int i=0; i<(pulseDuration*pulseFiringRate + 1); i++)
	{
		perturbFactor = 1 - perturbRate * 2 * (ranf() - 0.5);
		spikeTrain[2*eventCount] = fpgaInputAddr;
		spikeTrain[2*eventCount + 1] = pulseIsi * perturbFactor;
		eventCount++;
	}

	// double pulses - different inter-pulse intervals
	for (int j=0; j<6; j++)
	{
		// stimulation interval
		spikeTrain[2*eventCount] = fpgaInputAddr;
		spikeTrain[2*eventCount + 1] = stimulationInterval / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// first pulse of pair
		for (int i = 0; i < (pulseDuration*pulseFiringRate); i++)
		{
			perturbFactor = 1 - perturbRate * 2 * (ranf() - 0.5);
			spikeTrain[2*eventCount] = fpgaInputAddr;
			spikeTrain[2*eventCount + 1] = pulseIsi * perturbFactor;
			eventCount++;
		}

		// IPI from vector
		spikeTrain[2*eventCount] = fpgaInputAddr;
		spikeTrain[2*eventCount + 1] = IPIs[j] / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// second pulse of pair
		for (int i = 0; i < (pulseDuration*pulseFiringRate); i++)
		{
			perturbFactor = 1 - perturbRate * 2 * (ranf() - 0.5);
			spikeTrain[2*eventCount] = fpgaInputAddr;
			spikeTrain[2*eventCount + 1] = pulseIsi * perturbFactor;
			eventCount++;
		}
	}

	return(eventCount);
}

uint32_t genPoissonPulseSpikeTrain(uint16_t *spikeTrain, uint16_t inputAddr){

	// variables
	double varIsi = dt; // variable inter-spike interval [us] - min. value is one time step
	double x; // random number for probability test
	double t = dt; // time [s]
	uint16_t r = 500; // [Hz]
	uint32_t varStimCount; // variable stimulation event counter

	uint32_t eventCount = 0;
	uint16_t pulseIsi = (1/(float)pulseFiringRate) / (isiBaseVar/((float)clockFreq*1e6));

	float stimulationInterval = 250e-3; // [s]
	float IPIs[] = {1/(float)pulseFiringRate, 10e-3, 20e-3, 30e-3, 40e-3, 50e-3}; // [s]

	uint16_t fpgaInputAddr = (inputAddr & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// generate Poisson spike-pulse
	while (t <= pulseDuration)
	{
		x = ranf(); // [0,1)
		if (x <= r * dt)
		{
			// spike event
			spikeTrain[2*eventCount] = fpgaInputAddr;
			spikeTrain[2*eventCount + 1] = (varIsi) / (isiBaseVar/(clockFreq*1e6)); // isiFactor |isi = isiFactor * (isiBase/90) us
			eventCount++; // add event to counter
			varIsi = dt; // reset ISI variable to min. value
		}
		else
		{
			varIsi += dt; // no spike yet - increase ISI variable by one time step
		}
		t += dt; // update time
	}
	t = dt;
	varIsi = dt;

	// double pulses - different inter-pulse intervals
	for (int j=0; j<6; j++)
	{
		// stimulation interval
		spikeTrain[2*eventCount] = fpgaInputAddr;
		spikeTrain[2*eventCount + 1] = stimulationInterval / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// first pulse (Poisson)
		while (t <= pulseDuration)
		{
			x = ranf(); // [0,1)
			if (x <= r * dt)
			{
				// spike event
				spikeTrain[2*eventCount] = fpgaInputAddr;
				spikeTrain[2*eventCount + 1] = (varIsi) / (isiBaseVar/(clockFreq*1e6));
				eventCount++;
				varIsi = dt; // reset ISI variable
			}
			else
			{
				varIsi += dt; // no spike yet
			}
			t += dt; // update time
		}
		t = dt;
		varIsi = dt;

		// IPI from vector
		spikeTrain[2*eventCount] = fpgaInputAddr;
		spikeTrain[2*eventCount + 1] = IPIs[j] / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// second pulse (Poisson)
		while (t <= pulseDuration)
		{
			x = ranf(); // [0,1)
			if (x <= r * dt)
			{
				// spike event
				spikeTrain[2*eventCount] = fpgaInputAddr;
				spikeTrain[2*eventCount + 1] = (varIsi) / (isiBaseVar/(clockFreq*1e6));
				eventCount++;
				varIsi = dt; // reset ISI variable
			}
			else
			{
				varIsi += dt; // no spike yet
			}
			t += dt; // update time
		}
		t = dt;
		varIsi = dt;
	}

	return(eventCount);
}

uint32_t genConstantSpikeTrain(uint16_t *spikeTrain){
	//caerLog(CAER_LOG_NOTICE, __func__, "Started generating Poisson spike train.");

	uint32_t eventCount = 0;
#if CRICKET_NETWORK_1
	uint16_t fpgaAddr_in = (neuronAddr[inLF_L] & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	//printf("fpgaAddr_adj = %d, fpgaAddr_opp = %d.\n", fpgaAddr_adj, fpgaAddr_opp);

	// generate Poisson distributed spike events for low freq. stim.
	for (int i=0; i<100; i++){
		spikeTrain[2*i] = neuronAddr[inLF_L];
		spikeTrain[2*i + 1] = isiFactorFixed; // isi = isiFactor * (isiBase/90) us | compensate for stereo delay | can it be zero?
		eventCount++;
	}
#endif // CRICKET_NETWORK_1
	//caerLog(CAER_LOG_NOTICE, __func__, "Poisson stereo spike train generated.");
	return(eventCount);
}

uint32_t genPulsePair(uint16_t *spikeTrain, uint16_t inputAddr, bool doublePulse, float IPI){

	uint32_t eventCount = 0;
	uint16_t pulseIsi = (1/(float)pulseFiringRate) / (isiBaseVar/((float)clockFreq*1e6));

	float perturbFactor;

	float stimulationInterval = 250e-3; // [s]
	//float pulseIntervals[] = {1/(float)pulseFiringRate, 10e-3, 20e-3, 30e-3, 40e-3, 50e-3}; // [s]

	uint16_t fpgaInputAddr = (inputAddr & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// first pulse of pair
	for (int i = 0; i < (pulseDuration*pulseFiringRate + 1); i++)
	{
		perturbFactor = 1 - perturbRate * 2 * (ranf() - 0.5);
		spikeTrain[2*eventCount] = fpgaInputAddr;
		spikeTrain[2*eventCount + 1] = pulseIsi * perturbFactor;
		eventCount++;
	}

	if (doublePulse)
	{
		// IPI from function argument
		spikeTrain[2*eventCount] = fpgaInputAddr;
		spikeTrain[2*eventCount + 1] = IPI / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// second pulse of pair
		for (int i = 0; i < (pulseDuration*pulseFiringRate); i++)
		{
			perturbFactor = 1 - perturbRate * 2 * (ranf() - 0.5);
			spikeTrain[2*eventCount] = fpgaInputAddr;
			spikeTrain[2*eventCount + 1] = pulseIsi * perturbFactor;
			eventCount++;
		}
	}
	return(eventCount);
}

uint32_t genSingleSpike(uint16_t *spikeTrain, uint16_t inputAddr){

	//float stimulationInterval = 500e-3; // [s]
	//float isi = 80e-3;

	uint32_t eventCount = 0;

	uint16_t fpgaInputAddr = (1 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// single spike
	spikeTrain[2*eventCount] = fpgaInputAddr;
	spikeTrain[2*eventCount + 1] = 0;
	eventCount++;

	return(eventCount);
}

uint32_t genPolyPairTrain(uint16_t *spikeTrain){

	float stimInterval = 100e-3; // [s]
	float ISIs[] = {0, 1e-3, 2e-3, 3e-3, 4e-3, 5e-3, 6e-3, 7e-3, 8e-3, 9e-3, 10e-3}; // [s]
	uint32_t eventCount = 0;

	uint16_t fpgaInputAddr_1 = (1 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	uint16_t fpgaInputAddr_2 = (2 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	//  loop over ISIs
	for (int i = 0; i<11; i++)
	{
		// delay element #1
		spikeTrain[2*eventCount] = fpgaInputAddr_1;
		spikeTrain[2*eventCount + 1] = stimInterval / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// delay element #2
		spikeTrain[2*eventCount] = fpgaInputAddr_2;
		spikeTrain[2*eventCount + 1] = ISIs[i] / (isiBaseVar/(clockFreq*1e6));
		eventCount++;
	}

	return(eventCount);
}

uint32_t genPolyPair(uint16_t *spikeTrain, float polyISI)
{
	uint32_t eventCount = 0;

	uint16_t fpgaInputAddr_1 = (1 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	uint16_t fpgaInputAddr_2 = (2 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// delay element #1
	spikeTrain[2*eventCount] = fpgaInputAddr_1;
	spikeTrain[2*eventCount + 1] = 0;
	eventCount++;

	// delay element #2
	spikeTrain[2*eventCount] = fpgaInputAddr_2;
	spikeTrain[2*eventCount + 1] = polyISI / (isiBaseVar/(clockFreq*1e6));
	eventCount++;

	return(eventCount);
}

uint32_t genPolyTripletTrain(uint16_t *spikeTrain){

	float stimInterval = 100e-3; // [s]
	uint32_t eventCount = 0;
	float ISIs[11];
	float tempIsi = 0;

	for (int i = 0; i < 11; i++)
	{
		ISIs[i] = tempIsi;
		tempIsi += 0.5e-3;
	}

	// inh
	uint16_t fpgaInputAddr_1 = (1 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// exc
	uint16_t fpgaInputAddr_2 = (2 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	uint16_t fpgaInputAddr_3 = (3 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	uint16_t fpgaInputAddr_4 = (4 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// loop over ISIs
	for (int i = 0; i<11; i++)
	{
		// inh
		spikeTrain[2*eventCount] = fpgaInputAddr_1;
		spikeTrain[2*eventCount + 1] = stimInterval / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// exc #1
		spikeTrain[2*eventCount] = fpgaInputAddr_2;
		spikeTrain[2*eventCount + 1] = 0;
		eventCount++;

		// exc #2
		spikeTrain[2*eventCount] = fpgaInputAddr_3;
		spikeTrain[2*eventCount + 1] = ISIs[i] / (isiBaseVar/(clockFreq*1e6));
		eventCount++;

		// exc #3
		spikeTrain[2*eventCount] = fpgaInputAddr_4;
		spikeTrain[2*eventCount + 1] = ISIs[i] / (isiBaseVar/(clockFreq*1e6));
		eventCount++;
	}

	return(eventCount);
}

uint32_t genPolyTriplet(uint16_t *spikeTrain, float polyISI)
{
	uint32_t eventCount = 0;

	// inh
	uint16_t fpgaInputAddr_1 = (1 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// exc
	uint16_t fpgaInputAddr_4 = (2 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	uint16_t fpgaInputAddr_2 = (3 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	uint16_t fpgaInputAddr_3 = (4 & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;

	// inh
	spikeTrain[2*eventCount] = fpgaInputAddr_1;
	spikeTrain[2*eventCount + 1] = 0;
	eventCount++;

	// exc #1
	spikeTrain[2*eventCount] = fpgaInputAddr_2;
	spikeTrain[2*eventCount + 1] = 0;
	eventCount++;

	// exc #2
	spikeTrain[2*eventCount] = fpgaInputAddr_3;
	spikeTrain[2*eventCount + 1] = polyISI / (isiBaseVar/(clockFreq*1e6));
	eventCount++;

	// exc #3
	spikeTrain[2*eventCount] = fpgaInputAddr_4;
	spikeTrain[2*eventCount + 1] = polyISI / (isiBaseVar/(clockFreq*1e6));
	eventCount++;

	return(eventCount);
}

void printSpikeTrain(uint16_t *spikeTrain, uint32_t stimCount, char *fileName){
	//caerLog(CAER_LOG_NOTICE, __func__, "Started printing spike train.");

	FILE *spikeTrainFile = fopen(fileName,"w");

	for (int i=0; i<2*stimCount; i+=2){
		fprintf(spikeTrainFile, "%d, %d\n", spikeTrain[i], spikeTrain[i+1]);
	}
	fclose(spikeTrainFile);
	//caerLog(CAER_LOG_NOTICE, __func__, "Printing spike train finished.");

}

uint32_t variableIsiFileToSpikeTrain(uint16_t *spikeTrain, char* fileName) // from the fpgaSpikeGen module
{
	caerLog(CAER_LOG_NOTICE, __func__, "Started reading spike train from file.");

	//HWFilterState state = moduleData->moduleState;

	FILE *fp = fopen(fileName, "r");
	if (fp == NULL)
	{
		caerLog(CAER_LOG_ERROR, __func__, "Could not open variable ISI file\n");
		return;
	}
	// get the number of lines
	int c;
	int last = '\n';
	uint32_t lines = 0;

	while ((c = getc(fp)) != EOF)  /* Count word line endings. */
	{
		if (c == '\n')
			lines += 1;
		last = c;
	}
	if (last != '\n')
		lines += 1;
	// read the file again to get the events
	rewind(fp);
	for (uint32_t i = 0; i < 2*lines; i+=2)
	{
		fscanf(fp, "%hu, %hu\n", (uint16_t*)(spikeTrain + i), (uint16_t*)(spikeTrain + i + 1));
		//fscanf(fp, "%hu, %hu\n", spikeTrain[i], spikeTrain[i+1]);
	}
	fclose(fp);
	caerLog(CAER_LOG_NOTICE, __func__, "Spike train read from file.");
	return(lines - 1);
}

void writeSpikeTrainFixedIsi(){ // write spike train array for fixed ISI stimulus

#if CRICKET_NETWORK_1
	spikeTrainFixed[0] = (neuronAddr[inLF_L] & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4;
	spikeTrainFixed[1] = (neuronAddr[inLF_R] & 0xff ) << 6 | (fpgaDestinationCore & 0xf) | (fpgaVirtualSourceChip & 0x3) << 4; // spike train bit field
#endif // CRICKET_NETWORK_1
	caerLog(CAER_LOG_NOTICE, __func__, "Spike train for fixed ISI created.");
}


// DYNAP-SE MEASUREMENT FUNCTIONS

void monitorNeuron(optimizerState state, uint16_t neuronId){
	//caerLog(CAER_LOG_NOTICE, __func__, "monitorNeuron called.");

	uint8_t coreId = ((neuronId >> 8) & 0x03);
	uint32_t localNeuronId = (neuronId & 0xFF);

	caerDeviceConfigSet(state->eventSourceModuleState, DYNAPSE_CONFIG_MONITOR_NEU, coreId, localNeuronId);

	printf("monitoring:\tneuronId = %d, coreId = %d, localNeuronId = %d.\n", neuronId, coreId, localNeuronId);
}

uint16_t getGlobalX(uint16_t neuronId){ // get global Dynap-se x-coordinate from chip neuron-ID [0,1023]
	uint8_t chipId = 0;
	uint8_t coreId = ((neuronId >> 8) & 0x03);
	uint16_t columnId = (neuronId & 0x0F);
	bool addColumn = (coreId & 0x01);
	bool addColumnChip = (chipId & 0x01);
	columnId = U16T(columnId + (addColumn * DYNAPSE_CONFIG_NEUCOL) + (addColumnChip * DYNAPSE_CONFIG_XCHIPSIZE));

	return (columnId);
}

uint16_t getGlobalY(uint16_t neuronId){ // get global Dynap-se y-coordinate from chip neuron-ID [0,1023]
	uint8_t chipId = 0;
	uint8_t coreId = ((neuronId >> 8) & 0x03);
	uint16_t rowId = ((neuronId >> 4) & 0x0F);
	bool addRow = (coreId & 0x02);
	bool addRowChip = (chipId & 0x02);
	rowId = U16T(rowId + (addRow * DYNAPSE_CONFIG_NEUROW) + (addRowChip * DYNAPSE_CONFIG_YCHIPSIZE));

	return (rowId);
}

void printNetworkMeanrates(optimizerState state){
	uint16_t x;
	uint16_t y;
	float meanRate;
	int globalId;

	printf("\n");
	printf("Input neurons:\n");
	// loop over network neurons
	for (int i=0; i<numInput; i++){
		x = getGlobalX(neuronAddr[i]);
		y = getGlobalY(neuronAddr[i]);
		meanRate = state->frequencyMap->buffer2d[x][y];
		printf("meanRate[%d] = %f, ", neuronAddr[i], meanRate);
	}
	printf("\n\n");

	printf("Middle neurons:\n");
	// loop over network neurons
	for (int i=numInput; i<numInput+numMid; i++){
		x = getGlobalX(neuronAddr[i]);
		y = getGlobalY(neuronAddr[i]);
		meanRate = state->frequencyMap->buffer2d[x][y];
		printf("meanRate[%d] = %f, ", neuronAddr[i], meanRate);
	}
	printf("\n\n");

	printf("Motor neurons:\n");
	// loop over network neurons
	for (int i=numInput+numMid; i<numNeur; i++){
		x = getGlobalX(neuronAddr[i]);
		y = getGlobalY(neuronAddr[i]);
		meanRate = state->frequencyMap->buffer2d[x][y];
		printf("meanRate[%d] = %f, ", neuronAddr[i], meanRate);
	}
	printf("\n\n");
}

void printNetworkSpikeCounts(optimizerState state){
	uint16_t x;
	uint16_t y;
	uint32_t spikeCount;
	int globalId;

	printf("\n");
#if CRICKET_NETWORK_1
	if (stim_L){
		printf("stim_L.\n");
	}
	else if (stim_R){
		printf("stim_R.\n");
	}
#elif CRICKET_NETWORK_2
	if (stim_singlePulse){
		printf("stim_singlePulse.\n");
	}
	else if (stim_doublePulse){
		printf("stim_doublePulse.\n");
	}
#endif

	/*printf("Input neurons:\n");
	// loop over network neurons
	for (int i=0; i<numInput; i++){
		x = getGlobalX(neuronAddr[i]);
		y = getGlobalY(neuronAddr[i]);
		spikeCount = state->spikeCountMap->buffer2d[x][y];
		printf("spikeCount[%s] = %d, ", neuronNames[i], spikeCount);
	}
	printf("\n");*/

	printf("Middle neurons:\n");
	// loop over network neurons
	for (int i=numInput; i<numInput+numMid; i++){
		x = getGlobalX(neuronAddr[i]);
		y = getGlobalY(neuronAddr[i]);
		spikeCount = state->spikeCountMap->buffer2d[x][y];
		printf("spikeCount[%s] = %d, ", neuronNames[i], spikeCount);
	}
	printf("\n");

	printf("Motor neurons:\n");
	// loop over network neurons
	for (int i=numInput+numMid; i<numNeur; i++){
		x = getGlobalX(neuronAddr[i]);
		y = getGlobalY(neuronAddr[i]);
		spikeCount = state->spikeCountMap->buffer2d[x][y];
		printf("spikeCount[%s] = %d, ", neuronNames[i], spikeCount);
	}
	printf("\n\n");
}

void updateSpikeCount(optimizerState state){
	//caerLog(CAER_LOG_NOTICE, __func__, "updateFrequencyMap called.");

#if CRICKET_NETWORK_1
	// store and update spike count value for AN1 neurons
	spikeCount_L = (float) state->spikeCountMap->buffer2d[ getGlobalX(neuronAddr[AN1_L]) ][ getGlobalY(neuronAddr[AN1_L]) ]; // get current spike count value
	spikeCount_R = (float) state->spikeCountMap->buffer2d[ getGlobalX(neuronAddr[AN1_R]) ][ getGlobalY(neuronAddr[AN1_R]) ]; // get current spike count value

	if (stim_L){
		//countDiff_L = (spikeCount_L-spikeCount_R)/(spikeCount_L+spikeCount_R + 1e-6);
		targetDiff_L = SQUARED(targetCountAdj - spikeCount_L) + SQUARED(targetCountOpp - spikeCount_R);
		//printf("targetDiff_L = %f.\n", targetDiff_L);
	}
	else if (stim_R){
		//countDiff_R = (spikeCount_R-spikeCount_L)/(spikeCount_L+spikeCount_R + 1e-6);
		targetDiff_R = SQUARED(targetCountAdj - spikeCount_R) + SQUARED(targetCountOpp - spikeCount_L);
		//printf("targetDiff_R = %f.\n", targetDiff_R);
	}
#endif // CRICKET_NETWORK_1

#if CRICKET_NETWORK_2
	// store and update spike count value for feature detector network neurons
	spikeCount_LN2 = (float) state->spikeCountMap->buffer2d[ getGlobalX(neuronAddr[LN2]) ][ getGlobalY(neuronAddr[LN2]) ]; // get current spike count value
	spikeCount_LN3 = (float) state->spikeCountMap->buffer2d[ getGlobalX(neuronAddr[LN3]) ][ getGlobalY(neuronAddr[LN3]) ]; // get current spike count value
	spikeCount_LN4 = (float) state->spikeCountMap->buffer2d[ getGlobalX(neuronAddr[LN4]) ][ getGlobalY(neuronAddr[LN4]) ]; // get current spike count value

	if (stim_singlePulse)
	{
		targetDiff_singlePulse =
				SQUARED(targetCount_LN2_singlePulse - spikeCount_LN2) +
				SQUARED(targetCount_LN3_singlePulse - spikeCount_LN3) +
				SQUARED(targetCount_LN4_singlePulse - spikeCount_LN4);
	}
	else if (stim_doublePulse)
	{
		targetDiff_doublePulse =
				SQUARED(targetCount_LN2_doublePulse - spikeCount_LN2) +
				SQUARED(targetCount_LN3_doublePulse - spikeCount_LN3) +
				SQUARED(targetCount_LN4_doublePulse - spikeCount_LN4);
	}

	// write network spike counts to new line in file
	for (int i=numInput; i<numNeur; i++)
	{
		fprintf(spikeCountFile, "%d", state->spikeCountMap->buffer2d[getGlobalX(neuronAddr[i])][getGlobalY(neuronAddr[i])]);
		if (i < (numNeur - 1))
			fprintf(spikeCountFile, " ");
	}
	//fprintf(spikeCountFile, "\n");

#endif // CRICKET_NETWORK_2

#if PIR_MEASUREMENT

	uint8_t coreId = coreId_LN3;
	uint16_t neuronsToMeas = 256;

	// loop over receiving neurons
	for (uint16_t neuronId = coreId*256; neuronId < (coreId*256 + neuronsToMeas); neuronId++)
	{
		fprintf(spikeCountFile, "%d", state->spikeCountMap->buffer2d[getGlobalX(neuronId)][getGlobalY(neuronId)]);

		if ( neuronId < (coreId*256 + neuronsToMeas - 1) )
			fprintf(spikeCountFile, " ");
	}

#endif // PIR_MEASUREMENT

	// reset spike count map
	for (size_t x = 0; x < state->frequencyMap->sizeX; x++) {
		for (size_t y = 0; y < state->frequencyMap->sizeY; y++) {
			state->spikeCountMap->buffer2d[x][y] = 0;
		}
	}
	//caerLog(CAER_LOG_NOTICE, __func__, "updateFrequencyMap executed.");
}

void updateFrequencyMap(optimizerState state){
	//caerLog(CAER_LOG_NOTICE, __func__, "updateFrequencyMap called.");

	// loop over 2D-grid with all neurons
	for (size_t x = 0; x < state->frequencyMap->sizeX; x++) {
		for (size_t y = 0; y < state->frequencyMap->sizeY; y++) {
			state->frequencyMap->buffer2d[x][y] = (float) state->spikeCountMap->buffer2d[x][y] / measureMinTime;
			// reset count
			state->spikeCountMap->buffer2d[x][y] = 0;
		}
	}
	//caerLog(CAER_LOG_NOTICE, __func__, "frequencyMap updated.");

	// store and update meanrate value for left motor
	lastMeanRate_L = meanRate_L; // store last meanrate value
	//meanRate_L = state->frequencyMap->buffer2d[xMotor_L][yMotor_L]; // get current meanrate value
	lastRateDiff_L = rateDiff_L;
	rateDiff_L = (meanRate_L-meanRate_R)/(meanRate_L+meanRate_R);
	decrLeft = (rateDiff_L <= lastRateDiff_L); // check for decrease in meanrate

	// store and update meanrate value for right motor
	lastMeanRate_R = meanRate_R; // store last meanrate value
	//meanRate_R = state->frequencyMap->buffer2d[xMotor_R][yMotor_R]; // get current meanrate value
	lastRateDiff_R = rateDiff_R;
	rateDiff_R = (meanRate_R-meanRate_L)/(meanRate_L+meanRate_R);
	decrRight = (rateDiff_R <= lastRateDiff_R); // check for decrease in meanrate

	printf("\nmcPos = %d, stimCycles = %d.\n"
			"stim_L = %d, stim_R = %d.\n"
			"meanRate_L = %f meanRate_R = %f.\n",
			mcPos, stimCycles, stim_L, stim_R, meanRate_L, meanRate_R);

	if (stim_L){
		printf("rateDiff_L = %f\n", rateDiff_L);
	}
	else if (stim_R){
		printf("rateDiff_R = %f\n", rateDiff_R);
	}
	//caerLog(CAER_LOG_NOTICE, __func__, "updateFrequencyMap executed.");
}

void printTestMeanrates(optimizerState state){
	uint16_t x;
	uint16_t y;
	float meanRate;
	int globalId;

	uint8_t x2;
	uint8_t y2;
	float meanRate2;
	int globalId2;

	uint16_t neuronToMeasure = 10;

	printf("Core 0:\n");
	// loop over neurons
	for (uint16_t neuronId = 0*256; neuronId < 0*256+neuronToMeasure; neuronId++){
		x = getGlobalX(neuronId);
		y = getGlobalY(neuronId);
		meanRate = state->frequencyMap->buffer2d[x][y];
		printf("meanRate[%d] = %.0f, ", neuronId, meanRate);
	}
	printf("\n\n");

	printf("Core 2:\n");
	// loop over neurons
	for (uint16_t neuronId = 2*256; neuronId < 2*256+neuronToMeasure; neuronId++){
		x = getGlobalX(neuronId);
		y = getGlobalY(neuronId);
		meanRate = state->frequencyMap->buffer2d[x][y];
		printf("meanRate[%d] = %.0f, ", neuronId, meanRate);
	}
	printf("\n\n");

	printf("Core 3:\n");
	// loop over neurons
	for (uint16_t neuronId = 3*256; neuronId < 3*256+neuronToMeasure; neuronId++){
		x = getGlobalX(neuronId);
		y = getGlobalY(neuronId);
		meanRate = state->frequencyMap->buffer2d[x][y];
		printf("meanRate[%d] = %.0f, ", neuronId, meanRate);
	}
	printf("\n\n\n");

}
